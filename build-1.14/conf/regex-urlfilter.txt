# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# The default url filter.
# Better for whole-internet crawling.

# Each non-comment, non-blank line contains a regular expression
# prefixed by '+' or '-'.  The first matching pattern in the file
# determines whether a URL is included or ignored.  If no pattern
# matches, the URL is ignored.

# skip file: ftp: and mailto: urls
-^(file|ftp|mailto):

# skip too long urls:
-^.{200,}

# skip image and other suffixes we can't yet parse
# for a more extensive coverage use the urlfilter-suffix plugin
-\.(gif|GIF|jpg|JPG|png|PNG|ico|ICO|css|CSS|sit|SIT|eps|EPS|wmf|WMF|zip|ZIP|ppt|PPT|mpg|MPG|xls|XLS|xlsx|XLSX|gz|GZ|rpm|RPM|tgz|TGZ|mov|MOV|exe|EXE|jpeg|JPEG|bmp|BMP|js|JS|PDF|pdf|Pdf|doc|docx|svg|jar|ps|vcf|dat|asc|csv|tsv|pov)$

# skip URLs containing certain characters as probable queries, etc.
#-[?*!@=]
# Since most CMS systems query uncategorized articles with ?, it should be included.
# Stupidly, the website contains both ? and =, so that is really bad...
# Maybe ignore # for later thoughts
-[*!@]

# skip URLs with slash-delimited segment that repeats 3+ times, to break loops
-.*(/[^/]+)/[^/]+\1/[^/]+\1/

# skip anything related to search, since some of these URLs came ub for the subdomain already,
# since the = and ? are now allowed.
# Avoiding this can be helpful on the long run, since these pages usually take significant
# processing time

# exhaustive filtering after skimming through the results of a more extensive run.
-.*search.*
-.*\.js.*
-.*\.css.*
-.*order=.*
-.*=asc.*
-.*=desc.*
-.*time=.*
-.*search.*
-.*(S|s)uche.*
-.*edit.*
-.*jquery.*
-.*form.php.*
-.*(L|l)ogin.*
-.*embed(\&|\?).*
-.*.jar.*
-.*parameters.*
-.key=.*

# Also ignore mail clients
-.*mail.*\.uni-heidelberg.de/.*

# not exactly how to treat calendars yet, since they could include more reasonable entries,
# especially since the talks given usually represent the topics of people pretty well.
-.*date=.*
-.*week=.*
-.*month=.*
-.*year=.*
-.*(W|w)oche=.*
-.*(M|m)onat=.*
-.*(J|j)ahr=.*
-.*(calendar|kalender).*


# not sure why exactly, but excluded this from the more extensive run. 
# Probably because of crawl delays (see line 89).
#-.*hci.*

# accept anything else
#+.
# minimal working example on the informatik subdomain.
#+^http://([a-z0-9]*\.)*informatik\.uni-heidelberg.de/.*

# for more extensive testing, include ifi as well.
#+^http://(\w*\.)*(informatik|ifi)\.uni-heidelberg.de/.*

# run on math/physics/info domains. Have to add chairs manually...
# add iup if you want no life. (15 sec crawl delay) (and hci) (and thphys)
#+^(http|https)://((-|\w)*\.)*(mathi|math|match|iwr|mathcomp|physi|kip|ziti|mathinf|physik|informatik|ifi)\.(uni-heidelberg|uni-hd)\.de/.*

# final limitation. Take anything that is within the uni-heidelberg domain.
+^(http|https)://((-|\w)*\.)*(uni-heidelberg|uni-hd)\.de/.*

