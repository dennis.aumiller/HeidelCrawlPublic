#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 13 09:45:27 2017

@author: Dennis Aumiller
"""

import sys
from multiprocessing import Pool
import os
from SolrExtension import *
from functools import partial
from Query import Query
from Person import *
from Institute import *
import json
from urllib.request import HTTPError
#import pprint

#from Institute import *

def institute_hack(curr_dir):
    os.chdir("/home/dennis/HeidelCrawl/Extractors/")
    import Institute
    institutes = Institute.institute_reader()
    os.chdir(curr_dir)
    return institutes

def people_hack(curr_dir):
    os.chdir("/home/dennis/HeidelCrawl/Extractors/")
    import Person
    people = Person.people_reader()
#    Person.generate_all_names(people)
    os.chdir(curr_dir)
    return people

# Pickle sucks....
def transfer_people(people):
    new_people = [0] * len(people)
    for i,p in enumerate(people):
        new_people[i] = Person(p.identity, p.name, p.given_name, p.personal_status, 
                               p.title, p.status, p.functions, p.institutes)
        
    return new_people

def transfer_institutes(institutes):
    new_inst = [0] * len(institutes)
    for i,p in enumerate(institutes):
        new_inst[i] = Institute(p.identity, p.name, p.hyperlink)
    return new_inst

def storePositions(person, solr=SolrClient(), collection='collection1', batch_size=10000):
    """
        Desc:
        Will store the occurrences of "person" in each of the appearing sites.
        This allows for the later co-occurrence extractor to work on Solr instead of local files.
    
    Params:
        person (Person): Person object from the people_index. Must be a single object only!
        solr (SolrClient): Unless otherwise specified, the default SolrClient location will be used.
        collection (str): Default is the collection1.
        batch_size (int): Value for the batch size in the queries to come. By now this has not been evaluated
            on the performance against large indices!
    """
    names = " ".join(person.combinations)
    
    q = Query(solr, collection, names, 'id,content', tv=True,batch_size=batch_size, req_handler='tvrh')
    print("Query successfully returned")
    sys.stdout.flush()
#
    for batch in q:
        print(names)
        sys.stdout.flush()
        pages, positions, lengths = batch.get_tvrh_positions(person.name.lower())
        
        if(pages):
        
            update_string = '['
#            pprint.pprint(pages)
            for i, page in enumerate(pages):
                if i>0:
                    update_string += ' , '
                # this is the update string format for my update query function.
                # input not only the occurrences, but also the quantity of occs on one page
                # This equals the length of the positions.
                update_string += '{"id":"'+ page +'", "people":{"add":[['+ str(person.identity) + ',' + str(len(positions[i])) + ',' + str(lengths[i]) + ',' + str(positions[i]) + ']]}}'
                # enable this line to clear all changes done
#                update_string += '{"id":"'+ page +'", "people":{"set":null}}'
                
            update_string += ']'
#            print(update_string)
                
            try:
                solr.update(update_string,output=False)
            # if it doesn't work, add them individually, and exclude the ones which suck
            except HTTPError:
#                update_string = '['
#                for i, page in enumerate(pages):
#                    if ('"' not in page):
#                        if i>0:
#                            update_string += ' , '
##                    # this is the update string format for my update query function.
##                    # input not only the occurrences, but also the quantity of occs on one page
##                    # This equals the length of the positions. 
#
#                        update_string += '{"id":"'+ page +'", "people":{"add":[['+ str(person.identity) + ',' + str(len(positions[i])) + ',' + str(lengths[i]) + ',' + str(positions[i]) + ']]}}'
##                    # enable this line to clear all changes done
##                        update_string += '{"id":"'+ page +'", "people":{"set":null}}'
#
#                    # if you want to add individually
##                        update_string = '[{"id":"'+ page +'", "people":{"add":[['+ str(person.identity) + ',' + str(len(positions[i])) + ',' + str(lengths[i]) + ',' + str(positions[i]) + ']]}}]'
##                        print(update_string)
##                        solr.update(update_string,output=False)
#                update_string += ']'
#                print(update_string)
#                solr.update(update_string,output=False)
                for i, page in enumerate(pages):
                    update_string = '[id":"'+ page +'", "people":{"add":[['+ str(person.identity) + ',' + str(len(positions[i])) + ',' + str(lengths[i]) + ',' + str(positions[i]) + ']]}}]'
#                    update_string = '[{"id":' + page + '", "people":{"set":null}}]'
                    
                    try:
                        solr.update(update_string,output=False)
                    except HTTPError:
                        pass

def create_people_lookup(people, lookup_fn="lookup_people.json"):
    
    lookup = {}
    
    for person in people:
        lookup[person.unique_id] = person.given_name + " " + person.name
        lookup[person.identity] = person.given_name + " " + person.name
    
    with open(lookup_fn, 'w') as f:
        json.dump(lookup, f)


if __name__ == "__main__":
    # if no argument is passed, the default value will be the number of threads available (or cores?)
    # More specifically, the output from os.cpu_count() for Python 3.x
#    pool = Pool(12)
#    dir_path = os.path.dirname(os.path.realpath(__file__))
#    
#    institutes = institute_hack(dir_path)
#    people = people_hack(dir_path)
#    
#    people = transfer_people(people)
#    institutes = transfer_institutes(institutes)
#    
#    people_writer(people)
#    institute_writer(institutes)
    people = people_reader()
    institutes = institute_reader()
    print("Successfully read institutes and people")    
    generate_all_names(people)
    clear_duplicates(people)
    
    people_writer(people)
    # also build a lookup for the name of a person, both with unique identifier and total.
    create_people_lookup(people)
    sys.stdout.flush()
    # TODO: 
    # enable this for later. Also remember to turn of pages and positions as return values!
#    for i,p in enumerate(people):
#        print("Now processing {}".format(p.name))
#        storePositions(p)

    people = pool.map(storePositions, people)
    pool.close()
    pool.join()   
    print("Finishing up...")
    
