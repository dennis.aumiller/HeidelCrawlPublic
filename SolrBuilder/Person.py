#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 21 23:18:04 2017

@author: Dennis Aumiller
"""

import pickle

# https://stackoverflow.com/questions/18005172/get-the-object-with-the-max-attributes-value-in-a-list-of-objects
from operator import attrgetter

"""
    Better solution would be to simply store this in a dictionary.
    Although neither the dictionary nor the class allow for a simple sorting algorithm.
    Dictionaries have the additional problem of non-duplicate key entries.
    This seems like a bit of overkill for it, but hey
    #SoftwareEngineeringAtItsBest
"""

class Person:
    """
    Attributes:
        id: Unique identifier from LSF
        unique_id: This is used to keep a unique index when working with the multipartite graph.
        name: Family name of person
        given_name: Given name of person
        personal_status: Teaching personnel, administration,...
        title: Dr./Prof./...
        status: active/inactive employee
        functions: Their respective jobs ...
        institutes: ... at respective institutes
    """
            
    def __init__(self, identity, name, given_name, personal_status, title, status, functions, institutes):
        # possibly add for clearer managing
        self.identity = identity
        self.unique_id = "p" + "{0:05d}".format(self.identity)
        self.name = name
        self.given_name = given_name
        self.personal_status = personal_status
        self.title = title
        self.status = status
        self.functions = functions
        self.institutes = institutes
        
    def generate_names(self):
        """
        Desc:
            Will generate a list of names, which can be queried later on.
            The exclusive query for either (only) given name or last name.
            Maybe have to consider second names?
            
        Params:
            None
        """
        
        name_combinations = []
        
        """
        Funnily enough, Solr already implements a very robust search feature.
        This means, putting anything in " " phrases will already return a solid guess,
        since it does not care about the number of non-word characters in between two terms,
        e.g. the given name and family name.
        For an example, we want to illustrate the possible name queries:
            "Dennis Aumiller"
            "Aumiller Dennis"
            "D Aumiller"
            "Aumiller D"
            
            Combinations are extended/replaced for the following special cases:
                Person has a middle name:
                    Dennis Albert Aumiller
                    Dennis A Aumiller
                    Aumiller Dennis A
                    D A Aumiller
                Person has a double-name (connected by a -):
                    A-L Mustermann
                    Mustermann A-L
        """
        
        # Only match non word as intermediates to avoid conflicting queries.
        start = "\""
        end = "\""
        sep = " "
        
        
        # GivenName LastName -- Michael Gertz
        name_combinations.append(start + self.given_name + sep + self.name + end)
        name_combinations.append(start + self.name + sep + self.given_name + end)
        
        # there is actually a pretty high "hitrate" for the below two cases (~10%),
        # so better leave this out...
        # The problem I see is that many papers only list the initials of the first name,
        # and we might thus miss out on some of these...
        # Will throw them out later!
        # Initial GivenName LastName -- M Gertz
        name_combinations.append(start + self.given_name[0] + sep + self.name + end)
        name_combinations.append(start + self.name + sep + self.given_name[0] + end)
        
        # treat second name (if included)
        
        # combinations with "double-names" (e.g. Anna-Lena, Jean-Luc, ...)
        # theoretically, there is a chance that both occur...
        # Hint: This actually happens.
        # E.g. Hans-Peter Alois Müller
        
        if(" - " in self.given_name and " " in self.given_name):
            if (" - " in self.given_name):
                # special case where - is with blanks around.
                name_combinations.append(start + "-".join(self.given_name.split(" - ")) + sep + self.name + end)
                name_combinations.append(start + self.name + sep + "-".join(self.given_name.split(" - ")) + end)
        
        # "Double-names"
        elif("-" in self.given_name and len(self.given_name)-1 != self.given_name.rfind('-')):
            # Initials of TupleFirstName LastName -- L-M Huber (instead of Lisa-Marie Huber)
            # Despite this being quite likely (see first blocked out section), double names are rare enough to begin with.
            
            # may also contain multiple, thus slightly different approach
            name_combinations.append(start + "-".join([el[0] for el in self.given_name.split("-")]) + sep + self.name + end)
            name_combinations.append(start + self.name + sep + "-".join([el[0] for el in self.given_name.split("-")]) + end)
            
        # If the name of the person consists of multiple names (possibly second given name)
        elif(" " in self.given_name):
            # Only the first initial is already included by the second appendance
            
            # also include only the first name (until first " ")
            # Often only the first name is fully written -- Hans All (instead of Hans Peter All)
            name_combinations.append(start + self.given_name.split(" ")[0] + sep + self.name + end)
            name_combinations.append(start + self.name + sep + self.given_name.split(" ")[0] + end)
            
            # Only the additional given names are shorthand
            name_combinations.append(start + self.given_name.split(" ")[0] + sep +
                                     " ".join([el[0] for el in self.given_name.split(" ")[1:]]) + sep + self.name + end)
            name_combinations.append(start + self.name + sep + self.given_name.split(" ")[0] + sep +
                                     " ".join([el[0] for el in self.given_name.split(" ")[1:]]) + end)
            
            # same as above -- H P All (instead of Hans Peter All)
            # may also contain multiple, thus slightly different approach
            # Even here, the chances are quite low, so we can leave this in.
            name_combinations.append(start + " ".join([el[0] for el in self.given_name.split(" ")]) + sep + self.name + end)
            name_combinations.append(start + self.name + sep + " ".join([el[0] for el in self.given_name.split(" ")]) + end)

            
        self.combinations = name_combinations

def equals(person1, person2):
    if( person1.name == person2.name and
        person1.given_name == person2.given_name and
        person1.personal_status == person2.personal_status and
        person1.title == person2.title and
        person1.status == person2.status and
        person1.functions == person2.functions and
        person1.institutes == person2.institutes):
        return True
    else:
        return False

def generate_all_names(people_index):
    for i,p in enumerate(people_index):
        p.generate_names()

def clear_duplicates(people_index):
    # TODO: Finish this after cleaning all _exact_ duplicates....
    # There were only 46 or so exact duplicates, so better leave them in...
    
    # first get all short first name / lastname combinations
    short = [p.combinations[2] for p in people_index]
    dict_short = {}
    to_remove_short = []
    
    for i, name in enumerate(short):
        if name in dict_short:
            # also add the other occurrence, if it hasn't already been detected (multiple duplicates)
            if dict_short[name] not in to_remove_short:
                to_remove_short.append(dict_short[name])
                
            # and of course the actual occurrence
            to_remove_short.append(i)
        # otherwise just append to the dict for future reference
        else:
            dict_short[name] = i
        
    while (to_remove_short):
        # delete the third and fourth entry in the list, which are e.g., "M Gertz" and "Gertz M"
        del(people_index[to_remove_short.pop()].combinations[2:4])
    
    return people_index
        
        

# probably need to extend this feature?
def lookup_people_position(people_index):
    lookup = {}
    inv = {}
    
    for i, p in enumerate(people_index):
        lookup[p.identity] = i
        inv[i] = p.identity
    return lookup, inv

def people_writer(people_index, filename="people.pickle"):
    with open(filename, 'wb') as f:
        pickle.dump(people_index, f, protocol=pickle.HIGHEST_PROTOCOL)

def people_reader(filename="people.pickle"):
    with open(filename, 'rb') as f:
        people = pickle.load(f)
        
    return people

def get_offset(people_index):
    return max(people_index, key=attrgetter('identity')).identity