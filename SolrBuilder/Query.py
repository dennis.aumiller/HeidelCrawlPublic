#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 11 15:01:09 2017

@author: Dennis Aumiller
"""

from SolrExtension import *

class Query:
    
    """
#    TODO: Write explanation
#    This is the new one though
    
    
    def __init__(self, solr, collection, query_string, fields, tv=False, batch_size=10000,req_handler='select'):
        self.solr = solr
        self.collection = collection
        self.query_string = {'q':query_string}
        self.fields = {'fl':fields}
        self.batch_size = batch_size
        self.start_row = 0
        self.req_handler = req_handler
        
        self.dict = {**self.query_string, **self.fields, 'rows':self.batch_size, 'start':self.start_row}
        
        self.tv_dict = {}
        if(tv):
            self.tv_dict['tv'] = True
            self.tv_dict['tv.positions'] = True
            
        # immediately query for the results to evaluate the number of hits.
        self.response_size = self.solr.query(self.collection, 
                                             {**self.query_string,'fl':'','rows':0}).get_num_found()
        
    # return all results    
    def get_all(self, subfields=''):
        # workaround to accept subfields=self.fields
        if(subfields==''):
            return self.solr.query(self.collection, {**self.query_string, **self.fields, 
                                                 'rows':self.response_size, 'start':0, **self.tv_dict}, self.req_handler)
        else:
            return self.solr.query(self.collection, {**self.query_string, 'fl':subfields, 
                                                 'rows':self.response_size, 'start':0, **self.tv_dict}, self.req_handler)
    
        
    # define a Python iterator object and allow for a batch-wise query.
    def __iter__(self):
        return self
            
    def __next__(self):
        if(self.start_row > self.response_size):
            raise StopIteration
        else:
            self.start_row += self.batch_size
            return self.solr.query(self.collection, 
                                   {**self.query_string, **self.fields, 
                                    'rows':self.batch_size, 'start':self.start_row-self.batch_size,
                                    **self.tv_dict}, self.req_handler)
    # TODO: Implement batch-updater.
"""
    def __init__(self, solr, collection, query_string, fields, tv=False, batch_size=10000,req_handler='select'):
        self.solr = solr
        self.collection = collection
        self.query_string = query_string
        self.fields = fields
        self.batch_size = batch_size
        self.start_row = 0
        self.req_handler = req_handler
        self.tv = tv
        if (self.tv):
            self.tv_positions = True
        else:
            self.tv_positions = False
            
        self.response_size = self.solr.query(self.collection, 
                                             {'q':self.query_string,'fl':'','rows':0}).get_num_found()        
        
        
    def get_all(self, subfields=''):
        if(subfields==''):
            return self.solr.query(self.collection, {'q':self.query_string, 'fields':self.fields, 
                                                 'rows':self.response_size, 'start':0, 'tv':self.tv,
                                                 'tv.positions':self.tv_positions}, self.req_handler)
        else:
            return self.solr.query(self.collection, {'q':self.query_string, 'fl':subfields, 
                                                 'rows':self.response_size, 'start':0, 'tv':self.tv,
                                                 'tv.positions':self.tv_positions}, self.req_handler)
    
    # define a Python iterator object and allow for a batch-wise query.
    def __iter__(self):
        return self
            
    def __next__(self):
        if(self.start_row > self.response_size):
            raise StopIteration
        else:
            self.start_row += self.batch_size
            return self.solr.query(self.collection, 
                                   {'q':self.query_string, 'fl':self.fields, 
                                    'rows':self.batch_size, 'start':self.start_row-self.batch_size,
                                    'tv':self.tv, 'tv.positions':self.tv_positions}, self.req_handler)