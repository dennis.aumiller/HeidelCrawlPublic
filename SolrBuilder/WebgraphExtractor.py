#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 11 17:09:36 2017

@author: Dennis Aumiller
"""

"""
    Summary:
        Rewrote parts of the network extractor, and moved the occurrence part to another file
"""

from SolrExtension import *
from Query import Query
# may implement multicore support
#import multiprocessing
import numpy as np
import json
import multiprocessing as mp
import igraph

def batch_process(batch, ids, multi_edges, self_loops):
    print("Processing next batch...")
    curr_ids = batch.get_field_values_as_list('id')
    curr_outlinks = batch.get_field_values_as_list('outlinks')
    
    edgelist = np.empty([0,2], dtype=object)
    
    """
    The following iterations base on a few key assumptions:
    Since the (unique) id of each page can only appear once, the edge
    (curr_id -> any_other_page) can only come from this iteration.
    Thus it is not required to re-check the complete existing edgelist
    whether a edge is already existing in the first place.
    """
    
    for i, src_id in enumerate(curr_ids):
        curr_edgelist = np.zeros([len(curr_outlinks[i]),2], dtype=object)
        """
        The source id is always the index of the current page.
        Note that src_id is no actual integer, but a string!
        """
        curr_edgelist[:,0] = ids[src_id]
        for j, target_id in enumerate(curr_outlinks[i]):
            
            """
            This step is necessary since it seemed that some pages were
            not actually part of the database, but were still appearing
            in the outlinks of other pages. Not sure why, but this should keep it in check.
            TODO: Could implement a checker which counts the additional target counts.
            """

            if(target_id not in ids):
                target_id = target_id+"/"
                if (target_id in ids):
                    curr_edgelist[j,1] = ids[target_id]
#                try:
#                    # trailing / will be sometimes ignored, sometimes not...
#                    curr_edgelist[j,1] = ids[target_id+"/"]
#                except KeyError:
#                    pass
                # exclude this, since we do not want to respect these pages.
##                    return curr_ids, curr_outlinks, i, j
##                    print(curr_outlinks[i])
#                ids[target_id] = "w" + str(counter)
#                lookup["w" + str(counter)] = target_id
#                # Add to current edge list
#                curr_edgelist[j,1] = "w" + str(counter)
#                # Then increase to continue identifcation.
#                counter += 1
#            # The other case is easy and can be simply done by another access to the dict.
            else:
                curr_edgelist[j,1] = ids[target_id]
                
        """
        Check for self-loops or multi-edges. Note that these can only occur
        for the same outlink list. Thus, the adjacent pages are not relevant
        to this check (no cross-reference via edgelist)
        """
        
        if(curr_edgelist.any()):
            if not(multi_edges):
                # http://stackoverflow.com/questions/16970982/find-unique-rows-in-numpy-array
    #                b = np.ascontiguousarray(curr_edgelist).view(
    #                        np.dtype((np.void, curr_edgelist.dtype.itemsize * curr_edgelist.shape[1])))
    #                _, idx = np.unique(b, return_index=True)
    #    
    #                curr_edgelist = curr_edgelist[idx]
    #                    print(curr_edgelist)
                curr_edgelist = np.vstack({tuple(row) for row in curr_edgelist})
                
            if not(self_loops):
                self_indices = np.where(curr_edgelist[:,0]==curr_edgelist[:,1])
                curr_edgelist = np.delete(curr_edgelist, self_indices, axis=0)
                
                # since there are some pages that are pointed to,
                # but are not indexed, we may need to remove them.
                # Relates to line 50 ff.
            self_indices = np.where(curr_edgelist[:,1]=='0')
            curr_edgelist = np.delete(curr_edgelist, self_indices, axis=0)
#                
            """
            Sort the edges for more beauty.
            Since the individual problem is relatively small,
            this is super fast. Also note that this would return a wrong result
            if the values in the first column would actually differ, since this
            does sort each column individually. This is in-place,
            contrary to the .argsort() (which sorts by all columns, with respect to a target column)
            """
            
            curr_edgelist.sort(axis=0)
        
        edgelist = np.concatenate((edgelist, curr_edgelist))
    
    return edgelist

def webgraph_extractor(solr, collection, edge_fn="uni-hd.tsv", id_fn="id_mapping.json",
                       lookup_fn="lookup_mapping.json", size=10000, 
                       self_loops=False, multi_edges=False):
    """
    Desc:
        More mature version of the network extractor. Will serve as basis for
        the extraction of the other networks.
        Compared to the simple_extractor method, this does the generation with
        an incremental run on a variable batch size. Due to the incremental nature,
        the performance may be worse for smaller data sets due to overhead.
        
        Note that this is a directed multigraph, unless specified otherwise by parameters.        
    
    Params:
        solr (SolrClientHandler): SolrClient handler
        collection (str): Name of the collection in Solr.
        edge_fn (str): the filename (and/or directory) the edgelist file is going to be stored in.
        lookup_fn (str): See edge_fn, only for the mapping dictionary.
        lookup_fn (str): See above, but only for the lookup dictionary.
        size (int): Batch size that will be used for the generation process
        self_loops (bool): Whether or not self-looping edges are allowed.
        multi_edges (bool): Whether or not multi-edges are allowed.
    """
    """
    For the network extraction, start by caching the index of every page
    and assigning it a valid id number.
    """
    id_query = Query(solr, collection, '*:*', 'id', batch_size=100000)
    
#    # we assume this to be roughly 2.5-3m entries long, which roughly equals
#    # a size of around 300-500 MB in memory.
#    id_list = id_query.get_all('id').get_field_values_as_list('id')
    
    ids = {}
    lookup = {}
    
    counter = 0
    
    # batch-wise adding of all vertices to the vertex list.
    # The batch is necessary since Solr cannot handle too large request answers,
    # and we do not want to push our luck here...
    for batch in id_query:
        curr_ids = batch.get_field_values_as_list('id')
        for el in curr_ids:
            ids[el] = "w" + str(counter)
            lookup["w" + str(counter)] = el
            counter += 1
    
    print("Total of " + str(counter) + " unique ids initially found")
            
    # Now start with the actual generation of edges.
    
    edgelist = np.empty([0,2], dtype=object)
    
    outlink_query = Query(solr, collection, '*:*', 'id,outlinks', batch_size=size)
    
    pool = mp.Pool(12)
    queue = []
    for batch in outlink_query:
#        queue.append(batch_process(batch,ids, multi_edges, self_loops))
        queue.append(pool.apply_async(batch_process, [batch, ids, multi_edges, self_loops]))
                    
    while(queue):
#        res = queue.pop()
        res = queue.pop().get()
#        print(res)
        if(res.shape[0]>0):
            edgelist = np.concatenate((edgelist, res))
    pool.close()
    
    """
    Finally done with all the batch-processing. now have a final edgelist in memory,
    together with a id lookup table and the inverse address lookup table.
    Store them to files, respectively.
    """
    np.savetxt(edge_fn, edgelist, fmt="%s", delimiter='\t')
    
    with open(id_fn, 'w') as f:
        json.dump(ids, f)
    
    with open(lookup_fn, 'w') as g:
        json.dump(lookup, g)
    
    return edgelist, ids, lookup
    
def graph_measures(filename="uni-hd.tsv", diameter=True):
    g = igraph.Graph.Read_Ncol(filename, directed=True)
    
    # degree measures
    max_deg = g.maxdegree()
    print("The maximum degree is {}".format(max_deg))
    # TODO: Add average degree
    # TODO: Add degree distribution
    
    # diameter measures
    directed_diameter = g.diameter()
    print("The directed diameter is {}".format(directed_diameter))
    undirected_diameter = g.diameter(directed=False)
    print("The undirected diameter is {}".format(undirected_diameter))
    
    # clustering coefficient measures
    avg_lcc = g.transitivity_avglocal_undirected()
    print("The average local clustering coefficient is {}".format(avg_lcc))
    gcc = g.transitivity_undirected()
    print("The global clustering coefficient is {}".format(gcc))
    
    # largest connected component
    largest_conn_comp = g.components(mode= 'WEAK').giant().vcount()
    percent_largest = float(largest_conn_comp) / float(len(g.vs))
    print("The percentage of the largest connected component is {}".format(percent_largest))
    
    return g             
        
        
if __name__ == "__main__":
    solr = SolrClient()
    collection = 'collection1'
#    test, before = webgraph_extractor(solr, collection)
    edgelist, ids, lookup = webgraph_extractor(solr, collection)
    print("Finishing up....")
    
    
    
    
    
    