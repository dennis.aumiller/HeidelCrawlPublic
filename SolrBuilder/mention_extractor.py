#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 30 11:44:53 2017

@author: Dennis Aumiller
"""

from SolrExtension import *
from Query import Query
from Person import *
import collections
import operator


import numpy as np
import json
from itertools import groupby
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def where_are_they(name_lookup, ind_lookup, inv_ind_lookup, solr=SolrClient(), collection='collection1'):
    """
        Desc:
        Returns the number of occurrences. Since there will be further computations, 
        the total number of pages is also returned.
        Also returns the page with the maximum amount of people, and returns the pages id.
        
    
    Params:
        solr (SolrClient)
        collection (string)
    """    
    
    query = Query(solr, collection, '*:*', 'id,people')
    
    relevant_pages = 0
    total_pages = query.response_size
    total_distinct_occs = 0
    total_occs = 0
    max_pages = [("",0)] * 10
    max_total_pages = [("",0)] * 10
#    for i in range(10):
#        max_pages.append(("",0))
#        max_total_pages.append(("",0))
    bins = {}
    bins_total = {} 
    occ_person = [0] * 22187
    distinct_occ_person = [0] * 22187
    
    for batch in query:
        curr_people = batch.get_field_values_as_list('people')
        curr_ids = batch.get_field_values_as_list('id')
        
        for i, p_list in enumerate(curr_people):
            
            # list the individual degrees
            tmp_occs = 0
            for j, p in enumerate(p_list):
                tmp = eval(p)
                id_occ = int(tmp[0])
                freq = int(tmp[1])
                occ_person[ind_lookup[str(id_occ)]] += freq
                distinct_occ_person[ind_lookup[str(id_occ)]] += 1
                total_occs += freq
                tmp_occs += freq
                
            
            # for total degree dist
            try:
                bins_total[tmp_occs] += 1
            except KeyError:
                bins_total[tmp_occs] = 1
            # for site degree distribution
            try:
                bins[len(p_list)] += 1
            except KeyError:
                bins[len(p_list)] = 1
            if(p_list):
                relevant_pages += 1
                num_ppl = len(p_list)
                total_distinct_occs += num_ppl
                if(num_ppl > max_pages[9][1]):
                    for k, tup in enumerate(list(max_pages)):
                        if(num_ppl > tup[1]):
                            max_pages.pop()
                            max_pages.insert(k,(curr_ids[i],num_ppl))
                            break
                if(tmp_occs > max_total_pages[9][1]):
                    for k, tup in enumerate(list(max_total_pages)):
                        if(tmp_occs > tup[1]):
                            max_total_pages.pop()
                            max_total_pages.insert(k,(curr_ids[i],tmp_occs))
                            break
                    
                    
    # model as tuple list, to store more efficiently
    occ_person = [(inv_ind_lookup[str(i)], el) for i, el in enumerate(occ_person)]
    distinct_occ_person = [(inv_ind_lookup[str(i)], el) for i, el in enumerate(distinct_occ_person)]

                    
    # extract top 10 occurring people from list.
#    G_top_between = sorted(between_G.items(), key=operator.itemgetter(1), reverse=True)[:10]
    top_10_people = sorted(occ_person, key=operator.itemgetter(1), reverse=True)[:10]
    # write the top 10 pages and people
    with open("mention_info.txt", "w") as f:
        f.write("Below are the top 10 most frequently occurring people in the corpus.\n")
        for i, tup in enumerate(top_10_people):
            f.write(name_lookup[str(tup[0])] + " occurred " + str(tup[1]) + "\n")
    

    top_10_distinct_people = sorted(distinct_occ_person, key=operator.itemgetter(1), reverse=True)[:10]
    # write the top 10 pages and people
    with open("mention_info.txt", "a") as f:
        f.write("\nBelow are the top 10 most frequently _distinctly_ occurring people in the corpus.\n")
        f.write("That means only one occurrence per page is counted.\n")
        for i, tup in enumerate(top_10_distinct_people):
            f.write(name_lookup[str(tup[0])] + " occurred " + str(tup[1]) + "\n")
    
    
    # page stuff
    with open("mention_info.txt", "a") as f:
        f.write("\nThe following pages have the most distinct occurrences on them:\n" )
        for i, tup in enumerate(max_pages):
            f.write(str(tup[0]) + " has " + str(tup[1]) + " distinct occurrences" + "\n")
            
    with open("mention_info.txt", "a") as f:
        f.write("\nThe following pages have the most _total_ occurrences on them:\n")
        for i, tup in enumerate(max_total_pages):
            f.write(str(tup[0]) + " has " + str(tup[1]) + " total occurrences" + "\n")
    



    # print rest of relevant shit                

    relevant_fraction = float(relevant_pages) / float(total_pages)
    average_occs_overall = float(total_occs) / float(total_pages)
    average_occs_relevant = float(total_occs) / float(relevant_pages)
    
    with open("mention_info.txt", 'a') as f:
        f.write('{} pages contained at least one employee, which makes up for {:.2f}% \
of the total pages.\n'.format(relevant_pages, relevant_fraction*100))
        f.write('In addition, {} total occurrences have been registered, which makes \
for an average of {:.2f} per page.\n'.format(total_occs, average_occs_overall))
        f.write('Distributing the occurrences only over the pages actually containing \
 people, this makes for an average of {:.2f}.\n'.format(average_occs_relevant))
        f.write('The page with the most distinct people mentions is: {},\
cotaining {} occurrences.\n'.format(max_pages[0][0], max_pages[0][1]))
        f.write('The page with the most total entitiy mentions is: {},\
containing {} occurrences.\n'.format(max_total_pages[0][0], max_total_pages[0][1]))
        
    # plot mention distribution
    # and occ distribution
    
    # this is for pages
    x1 = [el[0] for el in bins.items()]
    y1 = [el[1] for el in bins.items()]
    
    x4 = [el[0] for el in bins_total.items()]
    y4 = [el[1] for el in bins_total.items()]
    
    # this for people
    all_occ_vals = sorted([x[1] for x in occ_person])
    x2 = [key for key, group in groupby(all_occ_vals)]
    y2 = [len(list(group)) for key, group in groupby(all_occ_vals)]
    
    distinct_occ_vals = sorted([x[1] for x in distinct_occ_person])
    x3 = [key for key, group in groupby(distinct_occ_vals)]
    y3 = [len(list(group)) for key, group in groupby(distinct_occ_vals)] 

    occurring_at_all = [el for i, el in enumerate(all_occ_vals) if el > 0]
    with open("mention_info.txt", 'a') as f:
        f.write("From the {} total people, {} do appear at least once. This means \
that {:.2f}% do not appear at all.\n".format(len(all_occ_vals),len(occurring_at_all), ((len(all_occ_vals)-len(occurring_at_all))/len(all_occ_vals)*100)))

    plt.figure(1)
    f1, axarr1 = plt.subplots(1,1)
    axarr1.loglog(x1, y1, linestyle='None', marker='x')
    axarr1.set_xlabel("# of dstinct occurrences on page")
    axarr1.set_ylabel("# of pages with x occurrences")
    axarr1.set_title("Distinct Occurrence Distribution over Websites")
    
    f4, axarr4 = plt.subplots(1,1)
    axarr4.loglog(x4,y4, linestyle='None', marker='x')
    axarr4.set_xlabel("# of total occurrences on page")
    axarr4.set_ylabel("# of pages with x occurrences")
    axarr4.set_title("Total Occurrence Distribution over Websites")
    
    f2, axarr2 = plt.subplots(1,1)
    axarr2.loglog(x2, y2, linestyle='None', marker='x')
    axarr2.set_xlabel("# of total occurrences of person")
    axarr2.set_ylabel("# of people with x occurrences")
    axarr2.set_title("Total Occurrence Distribution over People")
    
    f3, axarr3 = plt.subplots(1,1)
    axarr3.loglog(x3, y3, linestyle='None', marker='x')
    axarr3.set_xlabel("# of distinct occurrences of person")
    axarr3.set_ylabel("# of people with x occurrences")
    axarr3.set_title("Distinct Occurrence Distribution over People")
    
    f1.savefig("distinct_occ_pages.png")
    f2.savefig("distinct_occ_people.png")
    f3.savefig("total_occ_people.png")
    f4.savefig("total_occ_pages.png")

        
if __name__ == "__main__":
    
    with open("lookup_people.json", 'r') as f:
        name_lookup = json.load(f)
     
    with open("lookup.json", 'r') as f:
        ind_lookup = json.load(f)
        
    with open("inv_lookup.json", 'r') as f:
        inv_ind_lookup = json.load(f)
    
    solr = SolrClient()
    collection = 'collection1'


    where_are_they(name_lookup, ind_lookup, inv_ind_lookup)