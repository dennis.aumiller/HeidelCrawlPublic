#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 24 18:00:01 2017

@author: Dennis Aumiller
"""

import pickle
from Person import *
from Institute import *

if __name__ == "__main__":
    people = people_reader()
    print(len(people))
    with open("people_list.tsv", "w") as f:
        for i, p in enumerate(people):
            f.write(p.unique_id+"\n")
        