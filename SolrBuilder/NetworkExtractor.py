#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  3 19:54:17 2017

@author: Dennis Aumiller
"""

"""
THIS FILE IS OBSOLETE AND HAS A RENEWED VERSION OF ITS CONTENT IN WebgraphExtractor.py!


Two basic approaches are possible:
    Either the whole database is "downloaded", and stored to the file directly,
    which probably only works for relatively small graphs, or instances where
    the hardware is suited for such large scale operations;
    
    Or by simply ignoring this, and incrementally querying the extracted outlinks
    from the Solr database, by starting with an arbitrary first result.
    
    This approach follows a incremental solution, where piecewise batches are
    processed, and pieced together to a complete graph.
    
"""
from SolrExtension import *

import numpy as np
import math
import igraph

# Maybe TODO: Add query function...


def network_extractor(solr, collection,filename="uni-hd.tsv",
                      size=10000, self_loops=False, multi_edges=False):
    """
    Desc:
        More mature version of the network extractor. Will serve as basis for
        the extraction of the other networks.
        Compared to the simple_extractor method, this does the generation with
        an incremental run on a variable batch size. Due to the incremental nature,
        the performance may be worse for smaller data sets.
        Note that this is a directed multigraph.
    
    Params:
        solr (SolrClientHandler): SolrClient handler
        collection (str): Name of the collection in Solr.
        filename (str): the filename (and/or directory) the file is going to be stored in.
        size (int): Batch size that will be used for the generation process
        self_loops (bool): Whether or not self-looping edges are allowed.
        multi_edges (bool): Whether or not multi-edges are allowed.
        """
    
    test_response_size = solr.query(collection, {'q':'*', 'fl':'', 'rows':'0'})
    
    response_size = test_response_size.data['response']['numFound']
    
    if (response_size < size):

        runs = 1
    
    else:
        runs = math.ceil(float(response_size) / float(size))
    
    """
        Offset is necessary to account for pages without further links.
        These can be referenced in the outlinks of previous pages,
        but won't appear as a indexed page in Solr. Thus, the offset accounts for these
        "dead-end" pages. Start with 1 to avoid conflict.
    """
    offset = 1
    
    # Create lookup dictionary, and corresponding inverse dictionary.
    lookup = {}
    inverse = {}
    
    # edgelist to be stored to file later. Will be built incrementally.
    edgelist = np.empty([0,2], dtype=int)
    
    for run in range(runs):
    
        # get a batch of the query. Start with the value where previously left.
        start = str(run*size)
        links = solr.query(collection, {'q':'*','fl':'id,outlinks', 'rows':size, 'start':start})

        # note that this is the custom get_field_as_list function!
        ids = links.get_field_values_as_list('id')
        outlinks = links.get_field_values_as_list('outlinks')
    
        
        for i in range(len(ids)):
            """
                see whether or not the current ID is already in the dictionary.
                If not, add the responsible key to the dictionary,
                and also create the inverse lookup for future reference.
                """
            try:
                idx = lookup[ids[i]]
            except KeyError:
                idx = offset
                lookup[ids[i]] = idx
                inverse[idx] = ids[i]
                offset += 1
                
            tb_concat = np.zeros([len(outlinks[i]),2], dtype=int)
            tb_concat[:,0] = idx
            for j in range(len(outlinks[i])):
                """
                Similar to the current ID before, add now any key to the dict
                which is contained in the actual outlinks.
                """
                try:
                    idx = lookup[outlinks[i][j]]
                except KeyError:
                    idx = offset
                    lookup[outlinks[i][j]] = idx
                    inverse[idx] = outlinks[i][j]
                    offset += 1

                    
                tb_concat[j,1] = idx

            """
            Check for self-loops or multi-edges. Note that these can only occur
            for the same outlink list. Thus, the adjacent pages are not relevant
            to this check (no cross-reference via edgelist)
            """
            
            if not(multi_edges):
                # http://stackoverflow.com/questions/16970982/find-unique-rows-in-numpy-array
                b = np.ascontiguousarray(tb_concat).view(np.dtype(
                        (np.void, tb_concat.dtype.itemsize * tb_concat.shape[1])))
                _, idx = np.unique(b, return_index=True)
    
                tb_concat = tb_concat[idx]
                
            if not(self_loops):
                self_indices = np.where(tb_concat[:,0]-tb_concat[:,1]==0)
                tb_concat = np.delete(tb_concat, self_indices, axis=0)
                
            """
            Sort the edges for more beauty.
            Since the individual problem is relatively small,
            this is super fast.
            """
            tb_concat.sort(axis=0)
                
            edgelist = np.concatenate((edgelist, tb_concat))
            
    """
    Once all runs are done, store the edge list to file.
    Careful: This might be large for huge data sets!
    TODO: Maybe add an incremental write to file, too.
    """
    np.savetxt(filename, edgelist, fmt='%i', delimiter="\t")   
 
    return edgelist, lookup, inverse
    
# obsolete and should not be used. Use json file format instead.
def store_vertices(lookup, filename="vertex-hd.dict"):
    with open(filename, 'w') as f:
        f.write(str(lookup))
        
def load_vertices(filename="vertex-hd.dict"):
    with open(filename, 'r') as f:
        s = f.read()
        lookup = s.eval()
        
    return lookup

def graph_measures(filename="uni-hd.tsv", diameter=True):
    g = igraph.Graph.Read_Ncol(filename, directed=True)
    
    # degree measures
    max_deg = g.maxdegree()
    print("The maximum degree is {}".format(max_deg))
    # TODO: Add average degree
    # TODO: Add degree distribution
    
    # diameter measures
    directed_diameter = g.diameter()
    print("The directed diameter is {}".format(directed_diameter))
    undirected_diameter = g.diameter(directed=False)
    print("The undirected diameter is {}".format(undirected_diameter))
    
    # clustering coefficient measures
    avg_lcc = g.transitivity_avglocal_undirected()
    print("The average local clustering coefficient is {}".format(avg_lcc))
    gcc = g.transitivity_undirected()
    print("The global clustering coefficient is {}".format(gcc))
    
    # largest connected component
    largest_conn_comp = g.components(mode= 'WEAK').giant().vcount()
    percent_largest = float(largest_conn_comp) / float(len(g.vs))
    print("The percentage of the largest connected component is {}".format(percent_largest))
    
    return g
    
def co_occurrence_extractor(solr, collection, names):
    """
    Desc:
        Extracting the co-occurrences within the indexed pages.
        So far extracting only the pages in which the person appears.
        
    Params:
        solr (SolrClient): Handler for SolrClient
        collection (string): Name of the collection
        names (dict/list): List or dictionary of names to be extracted.
            For best performance, esp. long lists of names, use a dictionary.
    
    TODO: Try to evaluate whether it is faster to pre-assign a dict with
    dict.fromkeys(keys), or to increasingly build up a 
    """
    
    index = dict.fromkeys(names)
    constructor = {}

    for name in names:
        """
        TODO: Include batch processing for large chunks.
        Although these here are quite small, since only ID, and specific query.
        
        Also, care for the naming. This would best be done here, since we need to
        do this for every name. Can alternatively be done in advance, by preprocessing the names.
        """
        name_response = solr.query(collection, {'q':name, 'fl':'id', 'rows':'100000'})
        
        occurrences = name_response.get_field_values_as_list('id')
        
        """
        Maybe decide for either one of these methods later on.
        For now, test both, since we do not know which will prove to be more useful
        """
        
        index[name] = occurrences
        
        for occ in occurrences:
            try:
                constructor[occ].append(name)
            except KeyError:
                constructor[occ] = [name]
            
        
    return index, constructor
        
        

def co_occurence_distance_extractor(solr, collection, names, max_dist=5):
    """
    Desc:
        Extracting occurrences on the additonal premise that they appear within
        the range of max_dist sentences (or words)?
        
    Params:
        solr (SolrClient): Handler for SolrClient
        collection (string): Name of the collection
        names (dict/list): List or dictionary of names to be extracted.
            For best performance, esp. long lists of names, use a dictionary.
        max_dist (int): Not exactly sure how to treat this yet. The default in
            literature seems to be 5 sentences (but is it +/-, or only in one
            direction?). Yet, since the available splitting is not yet fixed,
            it may be more suitable to use a word length distance instead.
            This could also provide better results for pages with publications.
    """
    pass


# Helper function for the graph generation process. Only 
def cartesian(a,b):
    """
    Desc:
        Return the cartesian product without self-intersections of two lists.
        
    Params:
        a (list): List containing values.
        b (list): List containing values.
        
    """
    return ((a[i], b[j]) 
      for i in range(len(a))
      for j in range(len(b))
      if i != j)

def graph_builder(index, constructor, multi_edges=False, self_loops=False, edge_combine="sum"):
    """
    Desc:
        From a co-occurrence index, this function will build a graph in igraph.
        
    Params:
        index (dict): Keys are the names provided to co_occurrences, and values are
            lists of pages where these names appear.
        constructor (dict): Page index with those pages as keys, where at least
            one of the names is occurring. Currently, there are no empty pages!
        multi_edges (bool): Whether or not multi-edges will be allowed in the final graph.
        self_loops (bool): Whether or not self-loops will be allowed in the final graph.
        edge_combine (string): Can either be "sum" (sums up the edge weights for the collapsed
            edges) or "ignore" (which collapses to simply one edge). Is only relevant
            in the case of multi_edges = True.
    
    """
    g = igraph.Graph()
    
    # potential performance bottleneck
    for site in constructor:
        curr = constructor[site]
        if (len(curr) != 1):
            
            for el in constructor[site]:
                try:
                    g.vs.find(el)
                except ValueError:
                    g.add_vertex(el)
                
            """
            For now treat the relation of two people as bidirectional.
            Later, one could possible make the attention fixed
            
            Note that cartesian does not include self-relations
            """
            edge_sequence = list(cartesian(curr, curr))
            g.add_edges(edge_sequence)
    
    g.simplify(multiple=not(multi_edges),loops=self_loops,combine_edges="sum")
    
    return g
            
   

if __name__ == "__main__":
    solr = SolrClient("http://localhost:8983/solr")
    
    # default name
    collection = 'collection1'
    
    edgelist, lookup, inverse = network_extractor(solr, collection, "uni-hd.tsv", 10000)

    store_vertices(lookup, "vertex-hd.dict")
    
    """
    Manually create a comprehensive list of people that might show up.
    DBS: Michael Gertz, Andreas Spitz. Should have many co-occurrences
    Peter Bastian, because his name is ambiguous
    Barbara Paech, since she could have relations with Gertz
    Sadlo, as a member of the IWR. Might not show up on the current data.
    Bastian Rieck, might also not show up, but should have relations with Sadlo, if he does
    (Alexandra|Ulrich) Köthe, due to the ambiguous name in the same subdomain,
        and to see how Umlauts are treated natively, or rather how well.
        
    Importantly, Solr seems to be pretty robust with regard to upper/lowercase.
    This means it is only important to pass the correct expression to it,
    but it does not matter _how_ exactly it is written.
    """
    names = ['Gertz', 'Spitz', 'Paech', 'Sadlo', 'Rieck', 'Bastian', 'Köthe']
    
    index, constructor = co_occurrence_extractor(solr, collection, names)
    
#    g = graph_builder(index, constructor)
#    g = graph_measures("uni-hd.tsv")
    
    