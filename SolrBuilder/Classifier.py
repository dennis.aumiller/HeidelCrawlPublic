#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 25 11:51:06 2017

@author: dennis
"""

from SolrExtension import *
from Query import *
import numpy as np
from itertools import product
import sys

def generate_dataset(q,x, solr=SolrClient(), collection='collection1'):

    
    q = Query(solr, collection, q, fields='id,people')
    x_temp = np.zeros([q.response_size, 6])
    for batch in q:
        print("Processing next batch...")
        sys.stdout.flush()
        curr_people = batch.get_field_values_as_list('people')
        
        for i,mentions in enumerate(curr_people):
            if (mentions):
                # number of different people that are mentioned.
                # I assume no duplicates here!
                different_ppl = len(mentions)
                # the various attributes that we want to extract
                ids = [0] * different_ppl
                occs = [0] * different_ppl
                pos = [0] * different_ppl
                
                # counter for all occurrences of all people on this page.
                occ_count_page = 0
                for j, p in enumerate(mentions):
                    tmp = eval(p)
                    ids[j] = tmp[0]
                    occs[j] = tmp[1]
                    pos[j] = tmp[3]
                    
                    occ_count_page += len(pos[j])
                    
                co_occ_count = 1
                avg_dist = 0
                for j, outer in enumerate(pos):
                    for k, inner in enumerate(pos[j+1:], j):
                        if (ids[j] != ids[k]):
                            edges = [abs(a-b) for a,b in product(outer,inner)]
                            avg_dist += sum(edges)
                            co_occ_count += len(edges)
#                print(pos)
                # length is the same for any data. Since we know that there
                # is at least one element, choose this one.
                length = tmp[2]
                # average distance between two name mentions.
                est_dist = float(length) / float(occ_count_page)
                avg_dist = float(avg_dist)/float(co_occ_count)
            
            # unique, total, avg_mentions, length, avg_dist
                average_mentions = occ_count_page/different_ppl
                x_temp[i,:] = [different_ppl, occ_count_page, average_mentions, length, avg_dist, est_dist]
        
    x_temp = x_temp[np.any(x_temp, axis=1)]
    x_temp= np.concatenate((x_temp, np.zeros([x_temp.shape[0],1])+x), axis=1)
    return x_temp
        

if __name__ == "__main__":
# Publikationen
    q_pub = "id:/.*(P|p)ublication(s).*/ id:/.*(P|p)ublikation(en).*/"


# News
    q_news = "id:/.*(N|n)ews*/ id:/.*(N|n)euigkeiten.*/ id:/.*(N|n)eues.*/ id:/.*(A|a)ktuelles.*/"
    
# Mitarbeiter
    q_members = "id:/.*(M|m)itarbeiter.*/ id:/.*(employee(s)|member(s)).*/"
    
# Lehre
    q_teaching = "id:/.*(K|k)urse.*/ id:/.*(C|c)ourse(s).*/ id:/.*(L|l)ehre.*/ id:/.*(T|t)eaching.*/"
    
    
#    x_train = []
#    y_train = []
    
    x_pub = generate_dataset(q_pub, 1)
    # news
    x_news = generate_dataset(q_news, 2)
    # mitarbeiter
    x_members = generate_dataset(q_members, 3)
    # lehre
    x_teaching = generate_dataset(q_teaching, 4)
    
    min_length = min([x_pub.shape[0], x_news.shape[0], x_members.shape[0], x_teaching.shape[0]])
#    min_length = min([x_pub.shape[0], x_members.shape[0]])
    
    x_pub = x_pub[np.random.choice(x_pub.shape[0], min_length, replace = False)]
    
#    min_length = int(min_length/2)
    
    x_news = x_news[np.random.choice(x_news.shape[0], min_length, replace = False)]
    
    x_members = x_members[np.random.choice(x_members.shape[0], min_length, replace = False)]
    
    x_teaching = x_teaching[np.random.choice(x_teaching.shape[0], min_length, replace = False)]
    

    x_train = np.concatenate((x_pub, x_news, x_members, x_teaching))
#    x_train = np.concatenate((x_pub, x_members))
    np.random.shuffle(x_train)
    y = x_train[:,-1]
    x_train = x_train[:,:-1]
    
#    params = {'C': np.logspace(-3, 3, 10), 'gamma': np.logspace(-3, 3, 10),
#  'kernel': ['rbf', 'sigmoid'], 'class_weight':['balanced', None]}
#
#    clf = svm.SVC()   
#
#    random_search = RandomizedSearchCV(clf, params, n_iter = 20)
#    random_search.fit(x_naive, y)
#    report(random_search.cv_results_)   
    # tuned    
    
    np.save("x_train",x_train)
    np.save("y", y)
    
    print("Finished 4 class model...")
    sys.stdout.flush()
    
    x_pub = generate_dataset(q_pub, 1)
    # news
    x_news = generate_dataset(q_news, 2)
    # mitarbeiter
    x_members = generate_dataset(q_members, 2)
    # lehre
    x_teaching = generate_dataset(q_teaching, 2)
    
    min_length = int(x_pub.shape[0]/3)
 
    x_news = x_news[np.random.choice(x_news.shape[0], min_length, replace = False)]
    
    x_members = x_members[np.random.choice(x_members.shape[0], min_length, replace = False)]
    
    x_teaching = x_teaching[np.random.choice(x_teaching.shape[0], min_length, replace = False)]

    x_train = np.concatenate((x_pub, x_news, x_members, x_teaching))
#    x_train = np.concatenate((x_pub, x_members))
    np.random.shuffle(x_train)
    y = x_train[:,-1]
    x_train = x_train[:,:-1]

    
    np.save("x_binary", x_train)
    np.save("y_binary", y)
    print("Finishing up...")
    sys.stdout.flush()
