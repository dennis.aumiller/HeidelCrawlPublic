#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 17 12:50:38 2017

@author: Dennis Aumiller
"""

from multiprocessing import Pool
import os
from SolrExtension import *
from functools import partial
from Query import Query
from Person import *

import numpy as np
import json
from itertools import product
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def where_are_they(solr=SolrClient(), collection='collection1'):
    """
        Desc:
        Returns the number of occurrences. Since there will be further computations, 
        the total number of pages is also returned.
        Also returns the page with the maximum amount of people, and returns the pages id.
        
    
    Params:
        solr (SolrClient)
        collection (string)
    """    
    
    query = Query(solr, collection, '*:*', 'id,people')
    
    relevant_pages = 0
    total_pages = query.response_size
    total_occs = 0
    max_page = ''
    max_count = 0
    
    for batch in query:
        curr_people = batch.get_field_values_as_list('people')
        curr_ids = batch.get_field_values_as_list('id')
        
        for i, p_list in enumerate(curr_people):
            if(p_list):
                relevant_pages += 1
                num_ppl = len(p_list)
                total_occs += num_ppl
                if(num_ppl > max_count):
                    max_page = curr_ids[i]
                    max_count = num_ppl
                
        
    
    return relevant_pages, total_pages, total_occs, max_page, max_count


#def add_edges(edgelist, ids, people, counter):
#    for i, curr_id in enumerate(ids):
#        
#        if len(people[i] > 0):
#            curr = np.zeros([len(people[i]), len(people[i])], dtype = object)
#            curr[:,0] = 1
#            for j, person in people[i]:
#                pass
                
def write_edgelist(adj_mat, inv_lookup, occ_count_mat=np.zeros([0,0]),
                   avg_dist_mat=np.zeros([0,0]), min_occ=0, fn="people_co-occurrence_edgelist.tsv"):
    
    if(occ_count_mat.shape == (0,0)):
        occ_count_mat = adj_mat
        
        
    float_formatter = lambda x: "%.6f" % x
    print("Starting with edgelist...")
    sys.stdout.flush()
    edgelist = np.zeros([np.count_nonzero(occ_count_mat),3], dtype=object)
    counter = 0
    shape = adj_mat.shape
    

    
    for i in range(shape[0]):
        for j in range(shape[1]):
            if(occ_count_mat[i,j] > min_occ and adj_mat[i,j]>0):
                edgelist[counter,:] =  [str(inv_lookup[j]), str(inv_lookup[i]), float_formatter(adj_mat[i,j])]
                counter += 1
                
    # prunes the edgelist to the minimum number, if there are empty edges.
    edgelist = edgelist[~np.all(edgelist == 0, axis=1)]
                
        

    np.savetxt(fn, edgelist, fmt="%s", delimiter='\t')
    print("Finished writing edgelist.")
    sys.stdout.flush()
    
    print("Start with diagram...")
    sys.stdout.flush()
    

    if(avg_dist_mat.shape != (0,0)):
        normalized = avg_dist_mat.flatten()
        normalized = normalized[np.nonzero(normalized)]
        normalized.sort()
        norm_y = [(normalized.size-i)/normalized.size for i in range(normalized.size)]
        
        
    weights = edgelist[:,2]
    # is in place
    weights = weights.astype(float)
#    weights.sort()
    weights = np.sort(weights)
#    test = 0
#    for i in range(weights.size):
#        if weights[i] < test:
#            print("Error encountered")
#        else:
#            test = weights[i]
            
    y = [(weights.size-i)/weights.size for i in range(weights.size)]
    
    if(avg_dist_mat.shape != (0,0)):
        f, axarr = plt.subplots(4,1)
    else:
        f, axarr = plt.subplots(2,1)
    axarr[0].loglog(weights, y)
    axarr[1].semilogx(weights, y)
    
    
    if(avg_dist_mat.shape != (0,0)):
        axarr[2].loglog(normalized, norm_y)
        axarr[3].semilogx(normalized, norm_y)
        
    figname = fn[:-3]+"png"
    f.savefig(figname)
    
    print("Finished writing edge list images")
    sys.stdout.flush()
    
    
    

def co_occurrence_extractor(name_lookup, id_lookup, inv_id_lookup, all_occs, pori=True,
                            distance_weight="decay", window_size = 75, 
                            solr=SolrClient(), collection='collection1', batch_size=10000):
    """
        Desc:
        This will build the edgelist for the tripartite graph (Websites, Institutes, People)
        Either builds the people-webistes, or institute-websites graph, or at least that's what is intended.
        Also builds the people_people co-occurrences.
    
    Params:
        people_lookup (dict): The people index lookup; This is required since the occurrences are only based on the last name.
                To avoid distances computed between the same last names, the evaluation must only happen iff the last names
                of the two evaluated people are distinct.
        fn (string): The filename for the edgelist
        pori (bool): If set to True, will evaluate the "people" field. Otherwise "institutes"
        distance_weight (str): Can either be "attention_weight" or "attention_decay"
        window_size (int): Size of the window where the actual mention has to happen.
                            A window size of -1 is equal to no limit
        lookup (dict): Lookup for the person/institute, which id it has 
        inv_lookup (dict): The appropriate inverse lookup table.
        solr (SolrClient): Unless otherwise specified, the default SolrClient location will be used.
        collection (str): Default is the collection1.
        batch_size (int): Value for the batch size in the queries to come. By now this has not been evaluated
            on the performance against large indices!
    """

    if(pori):
        q = Query(solr, collection, '*:*', fields='id,people')
    else:
        q = Query(solr, collection, '*:*', fields='id,institutes', tv=False, batch_size=10000, req_handler='select')
        
        
    # this is very large; approximately 3.5-4 GB of size.
    # Yet the performance advantages of an adjacency matrix are just too good to neglect.
    number_people = int(float(len(name_lookup)) / 2.0)
    adj_mat = np.zeros([number_people, number_people])
    occ_count_mat = np.zeros([number_people, number_people])
    avg_dist_mat = np.zeros([number_people, number_people])
    
    
    for batch in q:
        print("Processing next batch...")
        sys.stdout.flush()
        curr_ids = batch.get_field_values_as_list('id')
        curr_people = batch.get_field_values_as_list('people')
        
        for i,mentions in enumerate(curr_people):
            # ID, frequency on this page, length of page, occs
            if (mentions):
                # number of different people that are mentioned.
                # I assume no duplicates here!
                different_ppl = len(mentions)
                # the various attributes that we want to extract
                ids = [0] * different_ppl
                occs = [0] * different_ppl
                pos = [0] * different_ppl
                
                # counter for all occurrences of all people on this page.
                occ_count_page = 0
                for j, p in enumerate(mentions):
                    tmp = eval(p)
                    ids[j] = tmp[0]
                    occs[j] = tmp[1]
                    pos[j] = tmp[3]
                    
                    occ_count_page += len(pos[j])
#                print(pos)
                # length is the same for any data. Since we know that there
                # is at least one element, choose this one.
                length = tmp[2]
                # average distance between two name mentions.
                avg_dist = float(length) / float(occ_count_page)
                
                for j, outer in enumerate(pos):
                    for k, inner in enumerate(pos[j+1:], j):
                    # this creates the distances between all occurrences
                    # of the different words.
                    # looks scary, but isn't.
                    # Need to string cast here, since it is re-read.
#                        print(outer, inner)
#                        print(j,k)
                        if (name_lookup[str(ids[j])] != name_lookup[str(ids[k])]):
                            edges = [abs(a-b) for a,b in product(outer,inner)]
                            curr_j = id_lookup[ids[j]]
                            curr_k = id_lookup[ids[k]]
                            

                            if(distance_weight == "attention_weight"):
                                min_dist = max(min(edges), 1) # safety reason, to avoid zero division
                                
                                if(window_size > 0):
                                    limit = float(min(window_size, avg_dist))
                                else:
                                    limit = avg_dist
                                if(min_dist < window_size):    
                                    weight_j = max(0, (1.0 - float(min_dist)/limit * occs[j]/different_ppl))
                                    weight_k = max(0, (1.0 - float(min_dist)/limit * occs[k]/different_ppl))
                                
                                    occ_count_mat[curr_j, curr_k] += 1
                                    occ_count_mat[curr_k, curr_j] += 1
                                
                                    adj_mat[curr_k, curr_j] += weight_j
                                    adj_mat[curr_j, curr_k] += weight_k
                                
                                    avg_dist_mat[curr_k, curr_j] += float(sum(edges)/len(edges))
                                    avg_dist_mat[curr_j, curr_k] += float(sum(edges)/len(edges))
                                
                            elif(distance_weight == "attention_decay"):
                                min_dist = max(min(edges), 1) # safety reason, to avoid zero division
                                
                                if(window_size > 0):
                                    limit = float(min(window_size, avg_dist))
                                else:
                                    limit = avg_dist
                                
                                if(min_dist < window_size):
                                    weight_j = np.exp(-(min_dist)/(limit/10.0)) * occs[j]/different_ppl
                                    weight_k = np.exp(-(min_dist)/(limit/10.0)) * occs[k]/different_ppl
                                    
                                    occ_count_mat[curr_j, curr_k] += 1
                                    occ_count_mat[curr_k, curr_j] += 1
                                    
                                    adj_mat[curr_k, curr_j] += weight_j
                                    adj_mat[curr_j, curr_k] += weight_k
                                    
                                    avg_dist_mat[curr_k, curr_j] += float(sum(edges)/len(edges))
                                    avg_dist_mat[curr_j, curr_k] += float(sum(edges)/len(edges))
                                
                            elif(distance_weight == "pure_occ"):
                                weight_j = len(edges)
                                weight_k = len(edges)
                                
                                occ_count_mat[curr_j, curr_k] += len(edges)
                                occ_count_mat[curr_k, curr_j] += len(edges)
                                
                                adj_mat[curr_k, curr_j] += weight_j
                                adj_mat[curr_j, curr_k] += weight_k
                                
                                avg_dist_mat[curr_k, curr_j] += float(sum(edges))
                                avg_dist_mat[curr_j, curr_k] += float(sum(edges))
                                
                            elif(distance_weight == "baseline"):
                                if(window_size > 0):
                                    limit = float(window_size)
                                else:
                                    limit = 50
                                    
                                for m, dist in enumerate(edges):
                                    if(dist<limit):
                                        weight_j = np.exp(-(dist/10.0))
                                        weight_k = np.exp(-(dist/10.0))
                                        
                                        occ_count_mat[curr_j, curr_k] += 1
                                        occ_count_mat[curr_k, curr_j] += 1
                                        
                                        adj_mat[curr_k, curr_j] += weight_j
                                        adj_mat[curr_j, curr_k] += weight_k
                                        
                                        avg_dist_mat[curr_k, curr_j] += dist
                                        avg_dist_mat[curr_j, curr_k] += dist
                                    

    print("Finished with main extraction loop. Averaging distances now...")
    sys.stdout.flush()

    avg_dist_mat = np.divide(avg_dist_mat, occ_count_mat)
    avg_dist_mat = np.nan_to_num(avg_dist_mat)
    
    return adj_mat, occ_count_mat, avg_dist_mat
                                
                                
                            
                            
def get_all_occs(people_index, solr=SolrClient(), collection='collection1'):
    all_occs = [0] * len(people_index)
    
    for i,p in enumerate(people_index):
        q = Query(solr, collection, p.combinations, 'id,content')
        all_occs[i] = q.response_size
    
    return all_occs
                

    
if __name__ == "__main__":
    
#    take the filename as the first argument.
    args = sys.argv
    if(len(args)>1):
#        print("Hello")
        filename = args[1]
    else:
        filename = "people_co_occ_edgelist.tsv"
#    after that comes the used model
    if(len(args)>2):
#        print("Hello")
        method = args[2]
    else:
        method = "attention_weight"
#    and then the window_size
    if(len(args)>3):
        window = int(args[3])
    else:
        window = 75
#    print(filename)
    all_filename = "all_"+filename
    limited_filename = "limited_"+filename
    normalized_filename = "normalized"+filename
    adj_mat_fn = "adj_"+filename[:-3] + "npy"
    occ_mat_fn = "occ_"+filename[:-3] + "npy"
    avg_mat_fn = "avg_"+filename[:-3] + "npy"
    existing = os.path.isfile(adj_mat_fn)
    
    solr = SolrClient()
    collection = 'collection1'
    
    if(method == "xyz"): # should be "baseline"
        relevant_pages, total_pages, total_occs, max_page, max_count = where_are_they(solr, collection)
        relevant_fraction = float(relevant_pages) / float(total_pages)
        average_occs_overall = float(total_occs) / float(total_pages)
        average_occs_relevant = float(total_occs) / float(relevant_pages)
        
        print('{} pages contained at least one employee, which makes up for {:.2f}% \
    of the total pages.'.format(relevant_pages, relevant_fraction*100))
        print('In addition, {} total occurrences have been registered, which makes \
    for an average of {:.2f} per page.'.format(total_occs, average_occs_overall))
        print('Distributing the occurrences only over the pages actually containing \
     people, this makes for an average of {:.2f}.'.format(average_occs_relevant))
        print('The page with the most people mentions is: {}, cotaining {} occurrences. \
    This is of course no 100% accurate measure, but gives a general idea!'.format(max_page, max_count))
        
    with open("lookup_people.json", 'r') as f:
        name_lookup = json.load(f)
        
    
    people = people_reader()
    print("Read people...")
    sys.stdout.flush()
    
    id_lookup, inv_id_lookup = lookup_people_position(people)
#    all_occs = get_all_occs(people)
    print("Analyzed all occurrences...")
    sys.stdout.flush()
    all_occs = 0
    
#    if(existing):
#        print("Found existing adjacency matrix")
#        sys.stdout.flush()
#        adj_mat = np.load(adj_mat_fn)
#        occ_count_mat = np.load(occ_mat_fn)
#        avg_dist_mat = np.load(avg_mat_fn)
#    else:
    print("Calculating adjacency matrix from scratch")
    sys.stdout.flush()
    adj_mat, occ_count_mat, avg_dist_mat = co_occurrence_extractor(name_lookup, id_lookup, inv_id_lookup, all_occs,
                                                 distance_weight=method, window_size=window)
    np.save(adj_mat_fn, adj_mat)
    np.save(occ_mat_fn, occ_count_mat)
    np.save(avg_mat_fn, avg_dist_mat)
    
    # write all the combinations!!    
#    write_edgelist(adj_mat, inv_id_lookup, occ_count_mat=occ_count_mat, min_occ=0 , fn=all_filename)
#    write_edgelist(adj_mat, inv_id_lookup, occ_count_mat=occ_count_mat, min_occ=1 , fn=limited_filename)
#    # how to determine this?
#    write_edgelist(adj_mat, inv_id_lookup, occ_count_mat=occ_count_mat, avg_dist_mat=avg_dist_mat, min_occ=0, fn=normalized_filename)
    
    print("Finishing up...")
    sys.stdout.flush()
    # The total number of co-occurrences is the sum over occ_count_mat
    # Adj mat should be converted to edge list and stored.