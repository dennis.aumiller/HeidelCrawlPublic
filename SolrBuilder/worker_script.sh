echo "Starting with attention linear model..."
python3 OccurrenceBuilder.py attention_linear_edgelist.tsv attention_weight 75
echo "Finished attention linear model."
echo "Starting with attention decay model..."
python3 OccurrenceBuilder.py attention_decay_edgelist.tsv attention_decay 75
echo "Finished attention decay model."
echo "Limiting window further for linear mode..."
python3 OccurrenceBuilder.py attention_linear_small_edgelist.tsv attention_weight 25
echo "Finishing up..."
