#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  3 08:53:06 2017

@author: Dennis Aumiller
"""

from SolrClient import SolrClient
from SolrClient import SolrResponse
import pysolr
from urllib import request
"""
To get this to work, the SolrClient plugin has to be slightly changed,
with the value get_field_values_as_list changed to    
    return [if field in doc doc[field] else [] for doc in self.docs]
since the original return value would not match in size.
    
"""
def get_field_values_as_full_list(self, field):
    return [doc[field] if field in doc else [] for doc in self.docs]

SolrResponse.get_field_values_as_list = get_field_values_as_full_list

"""
Also add an update function to replace/update the values of existing documents.
"""
def update(self, new_data, host="http://localhost:8983/solr/", collection="collection1", output=False):
    """
    Desc:
        Similar to other python/Solr frameworks, the update function can insert
        new values into the client. Yet, all the other frameworks make use of the
        simple insertion method, which does only allow for a replacement of options.
        This function should provide a update rule, which does not replace, but extend.
        Example curl code:
            curl -X POST -H 'Content-Type: application/json' 'http://localhost:8983/solr/collection1/update?commit=true' 
            --data-binary '[{"id":"4", "people":{"add":["Doc-1"]}}]'
        In this case, people would be extended by the value "Doc-1"
        Parameters for the data structure can be found here:
            https://cwiki.apache.org/confluence/display/solr/Updating+Parts+of+Documents
        The original update function with python is from Stackoverflow:
            https://stackoverflow.com/questions/15460538/convert-solr-curl-updatejson-syntax-to-python-using-urllib2
        
    Params:
        new_data (json): A JSON-structured content. Examples can be found in desc.
        host (string): URL to the Solr server. Better safe than sorry with manual specification,
            since this is not natively in SolrClient and may lead to errors.
            Needs to finish with a trailing /
        collection (string): Name of the collection; same reason as host.
            Trailing / will be added manually here, since the name is not really intuitive with /
    """
    
    # commit is necessary to immediately see the changes. Might be worth to check performance
    # of this on a larger corpus, to see how impactful it is.
    url = host+collection+"/update?commit=true"
#    print(url)
    
    # convert data to byte array.
    byte_data = new_data.encode()
    
    req = request.Request(url=url, data=byte_data)
    
    req.add_header('Content-type', 'application/json')
    
    f = request.urlopen(req)
    # enable the subsequent line to see the request answer.
    if(output):
        print(f.read())
    
SolrClient.update = update


def clear_collection(self, host="http://localhost:8983/solr", collection="collection1"):
    conn = pysolr.Solr(host)
    conn.delete(q="*:*")

SolrClient.clear_collection = clear_collection

def init_tvrh(self,data):
    self.data = data
    self.query_time = data['responseHeader']['QTime']
    self.header = data['responseHeader']

    if 'response' in data:
        self.grouped = False
        self.docs = data['response']['docs']
        if 'numFound' in data['response']:
            self.num_found = data['response']['numFound']

    elif 'grouped' in data:
        self.groups = {}
        self.grouped = True
        for field in data['grouped']:
            #For backwards compatability
            self.groups = data['grouped'][field]['groups']
            self.docs = data['grouped'][field]['groups']
    else:
        self.grouped = False
        self.docs = {}
    
    if 'termVectors' in data:
        self.tvrh_ids = []
        # ingore the first two lines, which are only 'uniqueKeyFieldName' and 'id'
        self.termVectors = data['termVectors'][2:]
        
SolrResponse.__init__ = init_tvrh

def traverse(o, tree_types=(list, tuple)):
    if isinstance(o, tree_types):
        for value in o:
            for subvalue in traverse(value, tree_types):
                yield subvalue
    else:
        if isinstance(o, int):
            yield o

def traverse_max(o, tree_types=(list, tuple)):
    maxi = 0
    if isinstance(o, tree_types):
        for value in o:
            for subvalue in traverse(value, tree_types):
                if isinstance(subvalue, int):
                    if subvalue > maxi:
                        maxi = subvalue
    else:
        if isinstance(o, int):
            if o > maxi:
                maxi = o
    return maxi

def set_tvrh_content(self, name):
    # Also modified the init function for this special case.
    # This assumes a query with tv=true&tv.positions=true and request handler tvrh.
    # This was written specifically for this, and has no other use.
    
    # Also note that this will only extract the relevant parts for _the search term_
    # Meaning we only extract the number of occurrences for the search keyword
    # and list the total number of words/phrases in the document.
    
    # every second line is the unique id of the document.
    self.tvrh_ids = self.termVectors[::2]
    # this is the other part, with the content + noise
    content = self.termVectors[1::2]
    # this is the actual content.
    # workaround for content-less pages necessary.
    content = [i[3] if len(i)>3 else [] for i in content]
    terms = [i[::2] for i in content]
    
    # this is really ugly, but the fastest way I would know.
    """ 
    Since Solr has its own splitting mechanism, I don't really know
    how it does split. Thus, to extract the length of each page, find the
    maximum value of a complete traversal... For each page...
    """
    self.tvrh_lengths = [traverse_max(pages) for pages in content]
    # also find the index of the only word that is really interesting for us.
    # need special attention for names with whitespaces...
#    if(" " not in name and "-" not in name):
#        try:
#            keyword_index = [term.index(name) for term in terms]
#        # this happens only if the found name happens to be in the url...
#        except ValueError:
#            keyword_index = [0] * len(terms)
#            for i, term in enumerate(terms):
#                if(name in term):
#                    keyword_index[i] = term.index(name)
#                else:
#                    # should we catch this later?
#                    keyword_index[i] = -1
#    # TODO: This is not the final solution to this problem, since it only delays the problem.
#    # Best would be to extract something that is most unique, but not for now.
#    elif(" " in name and "-" in name):
#        keyword_index = [term.index(name.split(" ")[0].split("-")[0]) for term in terms]
#    elif(" " in name):
#        keyword_index = [term.index(name.split(" ")[0]) for term in terms]
#    elif("-" in name):
#        keyword_index = [term.index(name.split("-")[0]) for term in terms]
    curr_name = name
    if(" " in name and "-" in name):
        curr_name = name.split(" ")[0].split("-")[0]
    elif(" " in name):
        curr_name = name.split(" ")[0]
    elif("-" in name):
        curr_name = name.split("-")[0]
        
    if("," in curr_name or "." in curr_name or ";" in curr_name or ":" in curr_name):
        curr_name = curr_name.strip(",.;:")

    
    try:
        keyword_index = [term.index(name) for term in terms]
    # this happens only if the found name happens to be in the url...
    except ValueError:
        keyword_index = [0] * len(terms)
        for i, term in enumerate(terms):
            if(name in term):
                keyword_index[i] = term.index(name)
            else:
                # should we catch this later?
                keyword_index[i] = -1
            
    # and extract the positions of that word.
    # wow
    positions = [content[i][key*2+1][1][1::2] if content[i] else [0] for i,key in enumerate(keyword_index)]
    # some explanation: The key * 2 is always the index, since key does skip every second position.
    # +1 since we do not want the id, but the content instead
    # [1::2] then again extracts only every second position, which exludes further strings.

    # further noise in this
    self.tvrh_positions = positions
    
SolrResponse.set_tvrh_content = set_tvrh_content
    
    
def get_tvrh_positions(self,name):
    if(not self.tvrh_ids):
        self.set_tvrh_content(name)
    return self.tvrh_ids, self.tvrh_positions, self.tvrh_lengths
    
SolrResponse.get_tvrh_positions = get_tvrh_positions
    
    
    
