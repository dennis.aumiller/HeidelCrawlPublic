#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 22 12:05:00 2017

@author: Dennis Aumiller
"""

import pickle

class Institute:
    """
    Attributes:
        id: Unique identifier from LSF
        unique_id: This is used to keep a unique index when working with the multipartite graph.
        name: The name displayed in LSF. This is a very inaccurate representation for institutes
        hyperlink: Sometimes works, sometimes doesn't. For now, it is more to show that it can actually exist,
            and might be helpful for future purposes.
    """
    
    def __init__(self, identity, name, hyperlink):
        self.identity = identity
        # this is the value according to the code.
        self.unique_id = "i" + "{0:06d}".format(self.identity)
        self.name = name
        self.hyperlink = hyperlink
        
def lookup_institute(identity):
    name = identity
    return name
        
def institute_writer(institute_index, filename="institutes.pickle"):
    with open(filename, 'wb') as f:
        pickle.dump(institute_index, f, protocol=pickle.HIGHEST_PROTOCOL)

def institute_reader(filename="institutes.pickle"):
    with open(filename, 'rb') as f:
        institutes = pickle.load(f)
        
    return institutes

def get_eid_list(institutes):
    eids = [0] * len(institutes)
    for i, institute in enumerate(institutes):
        eids[i] = institute.identity
        
    return eids

def purge_institutes(institutes, occurring_institutes):
    clean_index = []
    
    for i, institute in enumerate(institutes):
        if institute.identity in occurring_institutes:
            clean_index.append(institute)
            
    return clean_index
        
def not_occurring(institutes, occurring_institutes, fn="empty_institutes.list"):
            
    not_occurring_institutes = []
    for i, inst in institutes:
        if (inst.identity not in occurring_institutes):
            not_occurring.append(inst)
            
    with open(fn, "w") as f:
        for el in not_occurring_institutes:
            f.write(str(el.identity) + " " + str(el.name))
            
    return not_occurring_institutes