#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 24 18:00:01 2017

@author: Dennis Aumiller
"""

import pickle
from Person import *
from Institute import *
import json

if __name__ == "__main__":
    insts = institute_reader()
    all_insts = institute_reader("all_institutes.pickle")
    print(len(insts))
    print(len(all_insts))
    all_occ_ids = [0] * len(insts)
    with open("institute_list.tsv", "w") as f:
        for i, inst in enumerate(insts):
            all_occ_ids[i] = inst.unique_id
            f.write(inst.unique_id+"\n")
            
    with open("empty_institutes.tsv", "w") as g:
        for i,inst in enumerate(all_insts):
            if inst.unique_id not in all_occ_ids:
                g.write(str(inst.identity) + "\t" + inst.name + "\n")

    inst_dict = {}
    for i, inst in enumerate(insts):
        inst_dict[inst.unique_id] = inst.name
        
    with open("lookup_institutes.json", 'w') as f:
        json.dump(inst_dict, f)
        