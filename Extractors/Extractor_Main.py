#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  6 09:07:42 2017

@author: Dennis Aumiller
"""

from InstituteExtractor import main_institutes
from NameExtractor import main_names
from BipartiteExtractor import main_bipartite

if __name__ == "__main__":
    """
    Since the order of operations is important in this context (the institutes
    have to be present to generate a valid name list, since duplicates are eliminated
    during the process of generation), a automated main method is provided here.    
    
    During the process, both the institutes and names main functions will generate
    one pickle file each.
    The initial institute index will be replaced by a purged version after the names
    have been generated. To extract the full list, please consider manually calling
    the main_institutes() function.
    
    The bipartite graph generator will merge these to a edge list of the bipartite graph,
    and two edge lists of the one-mode projections onto the respective nodes 
    (institutes and people).
    """
    main_institutes()
    main_names()
    main_bipartite()