#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 21 20:50:14 2017

@author: Dennis Aumiller
"""

import requests
from lxml import html
import json

from Institute import Institute, institute_writer


def generate_institute_urls(prefix, base_url):
    """
    Desc:
        Generate a batch of urls which follow the pattern of LSF institute request.
        The alternative would be to crawl over the form, which seems more unreasonable
        
    Params:
        prefix (str): The string which contains the prefix for a valid URL display.
        base_url (str): The string which contains the URL of the list of institutions.
    """
    
    page = requests.get(base_url)
    tree = html.fromstring(page.content)
    
    length = len(tree.xpath('//*[contains(@class, "content")]/fieldset[2]/div'))
    urls = [0] * int(length/2.0)
    identities = [0] * int(length/2.0)
    
    # Only evaluate every second div, because only they contain the link.
    for i in range(2,length+1,2):
        curr_href = tree.xpath('//*[contains(@class, "content")]/fieldset[2]/div[{}]/a/@href'.format(i))
        
        # Hack
        # might be faster with rfind (see NameExtractor)
        # but it does already work, so i better leave it...
        search_string = "einrichtung.eid="
        string_pos = curr_href[0].find(search_string)
        start_pos = string_pos + len(search_string)
        
        length_id = curr_href[0][start_pos:].find("&")
        
        eid = curr_href[0][start_pos:start_pos+length_id]
        
        identities[int(i/2.0)-1] = int(eid)
        urls[int(i/2.0)-1] = prefix + str(eid)
        
    return urls, identities

def clean(unclean):
    return ' '.join(unclean.split())

def parse_institute(tree, identity):
    
    """ 
    Since this is only an extension to the crawling online, it would make sense
    to ignore any institutes that have no web-presence...
    
    """
    invalid = 0
    
    name = ""
    if (tree.xpath('//*[contains(@class, "content")]/div/form/h2/font')):
        name = clean(tree.xpath('//*[contains(@class, "content")]/div/form/h2/font')[0].text)
    
        
    hyperlink = ""
    
    layout = tree.xpath('//*[contains(@class, "content")]/div/form/table/caption')
    
    if (layout):
        # See whether the desired category is actually in here
        categories = [clean(el.text).lower() for el in layout]
        
    if("adresse" in categories):
        hyperlink = tree.xpath('//*[@headers="addres_8"]/a')
        if(hyperlink):
            hyperlink = clean(hyperlink[0].text)
        else:
            # maybe set to an invalid flag?
            hyperlink = ""
    elif("link" in categories):
        hyperlink = tree.xpath('//*[contains(@class, "content")]/div/form/table[{}]/tr[2]/td[2]/a'.format(categories.index("link")+1))
        if(hyperlink):
            hyperlink = clean(hyperlink[0].text)
        else:
            # maybe set to an invalid flag?
            hyperlink = ""
    
    institute = Institute(identity, name, hyperlink)
    
    return institute


def get_institutes(urls, identities):
    
    institute_index = [0] * len(urls)
    institute_lookup = {}
    
    for i, url in enumerate(urls):
        curr_page = requests.get(url)
        tree = html.fromstring(curr_page.content)
        
        institute_index[i] = parse_institute(tree, identities[i])
        institute_lookup[institute_index[i].identity] = institute_index[i].unique_id
        
    institute_writer(institute_index, "all_institutes.pickle")
    # notice that dumping as json will result in reading it as strings!
    with open('institute_lookup.json', 'w') as f:
        json.dump(institute_lookup, f)
        
    
    
        

def main_institutes():
    """ 
    The idea is to extract the 'einrichtung.id's for every institution.
    The hyperlinks available on that site contain all the necessary information for that,
    and can later be processed in similar fashion to the NameExtractor.
    
    Structurally similar to NameExtractor
    """

    # This site contains all the necessary information about any "institution" (not institute!)
    baseline = "https://lsf.uni-heidelberg.de/qisserver/rds?state=change&type=6&moduleParameter=einrichtungSelectohneSemesterbezug&nextdir=change&next=SearchSelect.vm&target=einrichtungSearch&subdir=einrichtung&init=y&source=state%3Dchange%26type%3D5%26moduleParameter%3DeinrichtungSearch%26nextdir%3Dchange%26next%3Dsearch.vm%26subdir%3Deinrichtung%26_form%3Ddisplay%26topitem%3Ddepartments%26subitem%3DsearchDepartments%26field%3DEinrichtung&targetfield=Einrichtung&_form=display"
    # This is the required string for the necessary layout
    prefix = "https://lsf.uni-heidelberg.de/qisserver/rds?state=verpublish&status=init&vmfile=no&moduleCall=webInfo&publishConfFile=webInfoEinrichtung&publishSubDir=einrichtung&keep=y&einrichtung.eid="
    
#    page = requests.get(baseline)
#    tree = html.fromstring(page.content)
    
    urls, identities = generate_institute_urls(prefix, baseline)
    
    get_institutes(urls, identities)
    

    


if __name__ == "__main__":
    main_institutes()
    # test
#    testpage = requests.get("https://lsf.uni-heidelberg.de/qisserver/rds?state=verpublish&status=init&vmfile=no&moduleCall=webInfo&publishConfFile=webInfoEinrichtung&publishSubDir=einrichtung&keep=y&einrichtung.eid=921929")
#    testtree = html.fromstring(testpage.content)
#    test = testtree.xpath('//*[contains(@class, "content")]/div/form/table/caption')

#    y = tree.xpath('//*[contains(@class, "content")]/fieldset[2]/div[{}]/a/@href'.format(2))
#    length = len(tree.xpath('//*[contains(@class, "content")]/fieldset[2]/div'))

    
    