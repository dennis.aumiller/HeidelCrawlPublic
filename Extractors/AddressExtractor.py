#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 20 14:02:42 2017

@author: Dennis Aumiller
"""

import requests
from lxml import html


from Address import Person, people_writer, json_writer
from Institute import Institute, institute_reader, institute_writer, get_eid_list, purge_institutes

import collections
import pickle

def generate_urls(prefix, pid_limit=50000):
    """
    Desc:
        Generate a batch of urls which follow the pattern of LSF person request.
        The alternative would be to crawl over the form, which seems more unreasonable
        
    Params:
        prefix (str): The string which will be used as a "prefix" for the complete URL
        pid_limit (int): The number of URLs that will be generated. Note that the generation
            process does start from 0, and go continuously to the set limit.
            For performance reasons, a exclusion of intermediate areas could be included.
            Currently, the IDs seem to go on until at least 22000, but are only fragmented in between.
    """
    
    urls = []
    for i in range(pid_limit):
        urls.append(prefix + str(i))
    
    return urls
        

def clean(unclean):
    return ' '.join(unclean.split())

def parse_person(tree, identity):
    """
    Desc:
        For a given html body of a LSF page, this function will extract the necessary information.
        Might add variable extraction methods (including jobs, description, etc.) later,
        but for now this is concentrated on the key aspects: Name, given name, institute, title (Dr./Prof.)
    
    Params:
        tree (lxml tree): This is the tree-like XML structure to be parsed.
        identity (string): the PID identifier for the person
    """
    invalid = False
    
    if (tree.xpath('//*[@headers="basic_1"]')):
        # maybe check the actual names, especially since we had some strange happenings...
        # Since they actually apprea so in LSF, maybe ignore here, and treat it specially later on..
        name = clean(tree.xpath('//*[@headers="basic_1"]')[0].text)
        # It may exist, but it may be empty.
        if (name == ""):
            invalid = True
    else:
        name = ""
        # If the last name is missing, then it has to be invalid
        invalid = True
        
        
    if (tree.xpath('//*[@headers="basic_3"]')):
        given_name = clean(tree.xpath('//*[@headers="basic_3"]')[0].text)
        if (given_name == ""):
            invalid = True
    else:
        given_name = ""
        # Also make this invalid, since we later explode the names to combinations.
        invalid = True
        
    if (tree.xpath('//*[@headers="basic_6"]')):
        personal_status = clean(tree.xpath('//*[@headers="basic_6"]')[0].text)
    else:
        personal_status = ""
        
    if (tree.xpath('//*[@headers="basic_7"]')):
        title = clean(tree.xpath('//*[@headers="basic_7"]')[0].text)
    else:
        title = ""
    
    if (tree.xpath('//*[@headers="basic_12"]')):
        status = clean(tree.xpath('//*[@headers="basic_12"]')[0].text)
    else:
        status = ""
    
    institutes = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/table[2]/tr/td[2]/a')
    institute_links = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/table[2]/tr/td[2]/a/@href')
    jobs = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/table[2]/tr/td[3]')
    functions = {}
    institute_list = {}
    
    # Assert equal length

    if (len(institutes) == len(jobs)):
        for i in range(len(institutes)):
            # check whether in one line are actually both values
            if(institutes[i].text and jobs[i].text 
               and clean(institutes[i].text) not in functions):
                functions[clean(institutes[i].text)] = clean(jobs[i].text)
            
            # get the eid
            # this assumes a clean link, which is not necessarily given...
            # Clean, since the eid is expected to be at the last position.
            try:
                eid = institute_links[i][institute_links[i].rfind("=")+1:]
            except IndexError:
                eid = -1
            
            if(eid!=-1 and eid not in institute_list):
                institute_list[eid] = institute_links[i]
       
    """ 
        New: Extract the address of the person as well
    """
    
    tables = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/table')
    pointer = -1
    # extract the corresponding talbe with address information.
    for i, table in enumerate(tables):
        if (table[0].text == "1. Anschrift"):
            pointer = i
    
    # if address exists, continue
    
    if (pointer == -1):
        invalid = True
    else:
        address = tables[pointer].getchildren()
        
        # this is nasty
        dienstzimmer = clean(address[1].getchildren()[1].text)
        telefonnummer = clean(address[1].getchildren()[3].text)
        gebaeude = clean(address[2].getchildren()[1].text)
        fax = clean(address[2].getchildren()[1].text)
        strasse = clean(address[3].getchildren()[1].text)
        email = clean(address[3].getchildren()[3].text)
        plz = clean(address[4].getchildren()[1].text)
        hyperlink = clean(address[4].getchildren()[3].text)
        ort = clean(address[5].getchildren()[1].text)
    


    # Done with processing part. Following is the writing part         
                    
    # finally add them
    if not(invalid):
        # add the institutes to the list if they were not previously known.
#        depth_links = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/div[1]/div/a/@href')
#        # only consider this for people where it is not empty.
#        if(depth_links):
#            for i, el in enumerate(depth_links):
#                indices = el[el.rfind("=")+1:].split("|")
#                indices = "hierarchy" + "".join(["["+el+"]" for el in indices]).strip(",")#
#                eval(indices)
                
                
        person = Person(identity, name, given_name, personal_status, title, status, functions, institute_list,
                        dienstzimmer, telefonnummer, gebaeude, fax, strasse, email, plz, hyperlink, ort)
    
        return person

    else:
        return -1
    

def clean_index(people_index, institute_ids):
    # since invalid requests are marked with -1, filter them out.
    people_index = list(filter(lambda line: line != -1, people_index))
    
    occurring_institutes = []
    
    for i, person in enumerate(people_index):
        temp = {}
        for key in person.institutes:
            if (key != ''):
                if (int(key) in institute_ids):
                    temp[int(key)] = person.institutes[key]
                
                    if int(key) not in occurring_institutes:
                        occurring_institutes.append(int(key)) 
        
        
        # we clean the institute list on this side with one sweep, too.
        person.institutes = temp


    
    return people_index, occurring_institutes



def get_people(urls, institute="institutes.pickle"):
    people_index = [0] * len(urls)
    
    for i, url in enumerate(urls):
        if i%100==0:
            print("Parsing URL {}".format(i))
        curr_page = requests.get(url)
        curr_tree = html.fromstring(curr_page.content)
        people_index[i]= parse_person(curr_tree, i)
#        print(i)

    institute_index = institute_reader(institute)
    institute_ids = get_eid_list(institute_index)
    

    # This clears the ambiguous short names (e.g., Dennis Aumiller -> D Aumiller
    #                                         and Daniel Aumiller -> D Aumiller)
    
    
    # This removes institutes that do not have any people assigned.
    # This may be critical in the later extraction phase of 
    clean_people_index, occurring_institutes = clean_index(people_index, institute_ids)
    
#    hierarchy_writer(hierarchy)
    # default pickle writer
#    people_writer(clean_people_index)
    # modified JSON writer
    json_writer(clean_people_index)
    

def main_names():
    baseline = "https://lsf.uni-heidelberg.de/qisserver/rds?state=verpublish&status=init&vmfile=no&moduleCall=webInfo&publishConfFile=webInfoPerson&publishSubDir=personal&keep=y&purge=y&personal.pid="
    
#    # reduced to 30k, after the initial run with 80k showed no person after 28657.
    urls = generate_urls(baseline, 30000)
#    
#    # this takes quite long!
    get_people(urls)  

if __name__ == "__main__":
    main_names()
    
    
    """ Minimum working example for single id"""  
    baseline = "https://lsf.uni-heidelberg.de/qisserver/rds?state=verpublish&status=init&vmfile=no&moduleCall=webInfo&publishConfFile=webInfoPerson&publishSubDir=personal&keep=y&purge=y&personal.pid="
   
    urls = generate_urls(baseline, 30000)
#    page = requests.get(urls[15072])
#    tree = html.fromstring(page.content)
#    test3 = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/table')
#    //*[@id="wrapper"]/div[2]/div[5]/div[2]/div/form/table[2]
#    test = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/div[1]/div/@style')
#    test2 = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/div[1]/div/a/@href')
#    test = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/table[2]/tr/td[2]/a/@href')
#    institutes = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/table[2]/tr/td[2]/a')
#    institute_links = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/table[2]/tr/td[2]/a/@href')
#    jobs = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/table[2]/tr/td[3]')
#    y = parse_person(tree, 7)  
    
    
#    test = tree.xpath('//*[@headers="basic_1"]')
#    test = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/table[2]')
#    test = tree.xpath('//*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form')
#    //*[@id="wrapper"]/div[2]/div[5]/div[1]/div/form/table[2]
#    test = tree.xpath('/html/body/div[2]/div[2]/div[5]/div[2]/div/form')
    
    