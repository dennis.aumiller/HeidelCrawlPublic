#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  3 17:43:17 2017

@author: Dennis Aumiller
"""

import igraph
import json

import numpy as np
import itertools as it
from operator import attrgetter
from Person import Person, people_reader, get_offset
from Institute import Institute, institute_reader



def get_edgelist(people, institute_lookup, numerical=False):
    """
    Desc:
        Will generate an edge list for the bipartite graph consisting of
        institutes and people. Can either be numerical or string literals.
    Params:
        institutes (list): Containing the institute classes; used for matching in later steps.
        people (list): List containing the people (current and former employees).
        numerical (bool): Whether or not the edge list will contain numerical values only.
            The values of the edge list will look as follows for the respective options:
                numerical=True:
                    people_id = [0,...,max(people.identity)]
                    institutes_id = [max(people.identity)+1,...,max(people.identity)+len(institutes)]
                    
                numerical=False:
                    people_id = [p+str(0),...,p+str(len(people))]
                    institute_id = [i+str(0),...,i+str(len(institutes))]
            These are all approximations though. In practice, the existing eid and pid values will
            be taken as the ID, but they are within that range.
    """
    if(numerical):
        institute_offset = get_offset(people)
#        institute_offset = 0
        dat = int
        
    else:
        dat = object
        # need to reformat the values for later comparison/sorting...
        # This basically extracts the largest identity, so that the number of leading 0s will equal
        # the total necessary number.
        #Make Python great again!
#        form = str(len(str(max(institutes, key=attrgetter('identity')).identity)))
#        form = '0'+form+'d'
        
        
    edgelist = np.empty([0,2], dtype=dat)
    for i in range(len(people)):
        """
        Similar to NetworkExtractor, add individual edgelists and concatenate
        them together. The problem is, that these might often be relatively small,
        thus the overhead is gaining a lot more in weight.
        """
        
        curr_edges = np.zeros([len(people[i].institutes),2], dtype=dat)
        if(numerical):
            curr_edges[:,0] = people[i].identity
        else:
            curr_edges[:,0] = people[i].unique_id
            
        for j, key in enumerate(people[i].institutes):
            if(numerical):
                curr_edges[j,1] = int(key) + institute_offset
            else:
#                TODO!
                curr_edges[j,1] = institute_lookup[str(key)]
        
        """
        do not need to exclude self-loops (obvious), but also no multi-edges,
        since the generation process of institutes works on dicts.
        BTW: This whole sorting only works because the value in the first column
        is always the same for these.. Generally, sort does sort both columns
        independently, thus generating "wrong" results.
        Note that the sorting on strings is obviously limited to the string length,
        and might thus return wrong orderings. Since the ordering is only of secondary
        role (for now), this can be ignored.
        """
        curr_edges.sort(axis=0)

        # remove institutes that have invalid link ids
        # Might be obsolete due to earlier check.
        if(numerical):
            self_indices = np.where(curr_edges[:,1]==-1)
        else:
            self_indices = np.where(curr_edges[:,1]=='i')
            
        curr_edges = np.delete(curr_edges, self_indices, axis=0)   
        
        #finalize by concatenating with full list.
        edgelist = np.concatenate((edgelist, curr_edges))

    # not required, %s does the trick for both formats
#    if(numerical):
#        np.savetxt("edgelist.tsv", edgelist, fmt='%i', delimiter="\t")
#    else:

    return edgelist
    



def one_mode_projection_people(edgelist, weights=True, weight_normalized=False):
    """
    Desc:
        Generating the one-mode projection of the bibartite institute/people graph,
        onto the people.
    Params:
        edgelist (np.array): Contains all the edges, without weights
        weights (bool): If true, a third column with the accumulated co-occurrence
            will be added as a weight.
        weight_normalized (bool): If true, the weights will be normalized according to
            https://toreopsahl.com/tnet/two-mode-networks/projection/, where the total
            weight is dependent on the number of people at the institute.
            Important: If weight_normalized is set to true, weights also have to be true!
    """

    if(not(weights) and weight_normalized):
        raise ValueError("weights and weight_normalized have to be in accordance!")
    
    # Since numpy manipulations are generally in place, use a copy.
    temp_edgelist = edgelist
    
    """
    Sort according to the inistitute values, present in the second column.
    Again, the exact ordering is not fully relevant, as long as the specific
    institute edges are grouped together. Then, for each institute, the
    possible 2-tuple permutations generated from the list of all participating people
    will deliver the edges to be added.
    If the edge is already present, the weights are updated accordingly.
    """
    temp_edgelist = temp_edgelist[temp_edgelist[:,1].argsort()]
    
    # this seems unreasonably hard to perform with numpy, thus preferring "default python"?
#    bipartite = np.empty([0,2], dtype=dat)
    bipartite = []
    
    # weights might be accumulated separately to ensure comparability.
    bipartite_weights = []
    
    to_connect = []
    prev = temp_edgelist[0,1]
    if(weight_normalized==False):
        for i, edge in enumerate(temp_edgelist):
            # as long as the institute is still the same, add new people.
            if(prev == temp_edgelist[i,1]):
                # add next member
                to_connect.append(temp_edgelist[i,0])
            
            else:
                if(len(to_connect) > 1):
                    # all members of an institute have been found, now merge them to edges.
                    news = list(it.combinations(to_connect, 2))
                    bipartite.extend(news)
                
                to_connect = []
            
            prev = temp_edgelist[i,1]
            
        # Afterwards, eliminate duplicates and count the number of occurrences.
        bipartite = np.array(bipartite)
        

        # Magic solution from Stackoverflow
        b = np.ascontiguousarray(bipartite).view(np.dtype((np.void,
                                bipartite.dtype.itemsize * bipartite.shape[1])))
        _, idx, counts = np.unique(b, return_index=True, return_counts=True)
        bipartite = bipartite[idx]
        if (weights):
            t = np.zeros([bipartite.shape[0],bipartite.shape[1]+1], dtype=object)
            t[:,:-1] = bipartite
            # assign the weights
            t[:,-1] = counts
            
            bipartite = t
        
    else:
        for i, edge in enumerate(temp_edgelist):
            if(prev == temp_edgelist[i,1]):
                # add next member
                to_connect.append(temp_edgelist[i,0])
                
            else:

                if(len(to_connect) > 1):
                    # all members of an institute have been found, now merge them to edges.
                    news = list(it.combinations(to_connect, 2))
                    for j in range(len(news)):
                        if news[j] in bipartite:
                            update = bipartite.index(news[j])
                            bipartite_weights[update] += 1.0/(float(len(to_connect)-1.0))
                        else:
                            bipartite.append(news[j])
                            bipartite_weights.append(1.0)
                
                to_connect = []
            
            prev = temp_edgelist[i,1]
            
        t = np.zeros([len(bipartite),3], dtype=object)
        t[:,:-1] = bipartite
        t[:,-1] = bipartite_weights
        
        bipartite = t

    # sort for more beauty. Since every value is a Numpy array, this works.
    bipartite = bipartite[bipartite[:,0].argsort()]       
    return bipartite  

    

def one_mode_projection_institutes(edgelist, numerical=False, offset=0, weights=True, weight_normalized=True):    
    """
    Desc:
        Generating the one-mode projection of the bibartite institute/people graph,
        onto the institutes.
    Params:
        edgelist (np.array): Contains all the edges, without weights
        numerical (bool): Whether or not the input edgelist has numerical vertex identifiers
        offset (integer): Only required for numerical edge lists; This parameter is
            necessary since institutes are "shifted" with a offset during the edge list
            generation, and a clear identity should be maintained during the projection.
            Important: Offset will only be recognized if numerical is set to False!
        weights (bool): If true, a third column with the accumulated co-occurrence
            will be added as a weight.
        weight_normalized (bool): If true, the weights will be normalized according to
            https://toreopsahl.com/tnet/two-mode-networks/projection/, where the total
            weight is dependent on the number of shared institutes by a person.
            Important: If weight_normalized is set to true, weights also have to be true!
    """
    
    if(not(weights) and weight_normalized):
        raise ValueError("weights and weight_normalized have to be in accordance!")
        
    # Since numpy manipulations are generally in place, use a copy.
    temp_edgelist = edgelist
    
    """
    The code for this is more or less the same as for the institutes.
    Generally, only the columns should be "flipped".
    
    Again, the exact ordering is not fully relevant, as long as the specific
    institute edges are grouped together. This is inherently wrong!
    Need to change this ASAP!
    Then, for each institute, the possible 2-tuple permutations generated from 
    the list of all participating people will deliver the edges to be added.
    If the edge is already present, the weights are updated accordingly.
    """
    
    # here it is just a safety mechanism. Generally the generated edgelist is
    # already sorted, but just to be sure...
    temp_edgelist = temp_edgelist[temp_edgelist[:,0].argsort()]
    
    # this seems unreasonably hard to perform with numpy, thus preferring "default python"?
#    bipartite = np.empty([0,2], dtype=dat)
    bipartite = []
    
    # weights might be accumulated separately to ensure comparability.
    bipartite_weights = []
    
    
    to_connect = []
    prev = temp_edgelist[0,0]
    if(weight_normalized==False):
        for i, edge in enumerate(temp_edgelist):
            # as long as the person is still the same, add new institutes.
            if(prev == temp_edgelist[i,0]):
                # add next member
                if(numerical):
                    # if it is numerical, subtract the offset to keep consistent ids.
                    to_connect.append(temp_edgelist[i,1]-offset)
                else:
                    # otherwise, the names will be unique with with "i931019"
                    to_connect.append(temp_edgelist[i,1])
            else:
                if(len(to_connect) > 1):
                    # all members of an institute have been found, now merge them to edges.
                    news = list(it.combinations(to_connect, 2))
                    bipartite.extend(news)
                
                to_connect = []
            
            prev = temp_edgelist[i,0]
            
        # Afterwards, eliminate duplicates and count the number of occurrences.
        # Be aware that this is consistent with regard to the data type, i.e.
        # a numerical id will result in "numerical weights". Since no mathematical
        # operation is performed, it should still work.
        bipartite = np.array(bipartite)
        
        # Magic solution from Stackoverflow
        b = np.ascontiguousarray(bipartite).view(np.dtype((np.void,
                                bipartite.dtype.itemsize * bipartite.shape[1])))
        _, idx, counts = np.unique(b, return_index=True, return_counts=True)
        bipartite = bipartite[idx]
        if (weights):
            t = np.zeros([bipartite.shape[0],bipartite.shape[1]+1], dtype=object)
            t[:,:-1] = bipartite
            # assign the weights
            t[:,-1] = counts
            bipartite = t
        
    else:
        for i, edge in enumerate(temp_edgelist):
            if(prev == temp_edgelist[i,0]):
                # add next member
                if(numerical):
                    # if it is numerical, subtract the offset to keep consistent ids.
                    to_connect.append(temp_edgelist[i,1]-offset)
                else:
                    # otherwise, the names will be unique with with "i931019"
                    to_connect.append(temp_edgelist[i,1])
                
            else:
                if(len(to_connect) > 1):
                    # all members of an institute have been found, now merge them to edges.
                    news = list(it.combinations(to_connect, 2))
                    
                    for j in range(len(news)):
                        if news[j] in bipartite:
                            update = bipartite.index(news[j])
                            bipartite_weights[update] += 1.0/(float(len(to_connect)-1.0))
                        else:
                            bipartite.append(news[j])
                            bipartite_weights.append(1.0)
                
                to_connect = []
            
            prev = temp_edgelist[i,0]
            
        # Similar to the other branch, this will work regardless of 
        t = np.zeros([len(bipartite),3], dtype=object)
        t[:,:-1] = bipartite
        t[:,-1] = bipartite_weights
        
        bipartite = t
        
    # sort for more beauty. Since every value is a Numpy array, this works.
    bipartite = bipartite[bipartite[:,0].argsort()]
    
    return bipartite
    
def main_bipartite():
    institutes = institute_reader()
    people = people_reader()
    
    with open('institute_lookup.json', 'r') as f:
        institute_lookup = json.load(f)
    
    edgelist = get_edgelist(people, institute_lookup, numerical=False) 
    np.savetxt("edgelist.tsv", edgelist, fmt='%s', delimiter="\t")
    
    people_projection = one_mode_projection_people(edgelist,weights=True, weight_normalized=True)
    np.savetxt("people_projection.tsv", people_projection, fmt='%s', delimiter="\t")
    
    institutes_projection = one_mode_projection_institutes(edgelist,offset=get_offset(people), numerical=False,
                                                           weights=True, weight_normalized=True)
    np.savetxt("institutes_projection.tsv", institutes_projection, fmt='%s', delimiter="\t")
    

if __name__ == "__main__":
    
    """
    Not quite sure whether this is the final layout.
    Currently, all files have to be in the same (cluttered) folder,
    since otherwise importing doesn't correctly work.
    """
    main_bipartite()
    
    

    # pytest

            
            
        