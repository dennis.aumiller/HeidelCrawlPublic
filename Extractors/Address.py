#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 16:23:33 2017

@author: dennis
"""

import pickle
import json

class Person:
    """
    Attributes:
        id: Unique identifier from LSF
        unique_id: This is used to keep a unique index when working with the multipartite graph.
        name: Family name of person
        given_name: Given name of person
        personal_status: Teaching personnel, administration,...
        title: Dr./Prof./...
        status: active/inactive employee
        functions: Their respective jobs ...
        institutes: ... at respective institutes
    """
            
    def __init__(self, identity, name, given_name, personal_status, title, status, functions, institutes,
                 dienstzimmer, telefonnummer, gebaeude, fax, strasse, email, plz, hyperlink, ort):
        # possibly add for clearer managing
        self.identity = identity
        self.unique_id = "p" + "{0:05d}".format(self.identity)
        self.name = name
        self.given_name = given_name
        self.personal_status = personal_status
        self.title = title
        self.status = status
        self.functions = functions
        self.institutes = institutes
        self.dienstzimmer = dienstzimmer
        self.telefonnummer = telefonnummer
        self.gebaeude = gebaeude
        self.fax = fax
        self.strasse = strasse
        self. email = email
        self. plz = plz
        self.hyperlink = hyperlink
        self.ort = ort
        
def json_writer(people_index, filename="address.json"):
#    json_string = json.dumps([el.__dict__ for el in people_index])
    with open(filename, 'w') as out:
        json.dump([el.__dict__ for el in people_index], out)
    
        
def people_writer(people_index, filename="people.pickle"):
    with open(filename, 'wb') as f:
        pickle.dump(people_index, f, protocol=pickle.HIGHEST_PROTOCOL)

def people_reader(filename="people.pickle"):
    with open(filename, 'rb') as f:
        people = pickle.load(f)
        
    return people
