# Overview
This page is dedicated to the gathered resources during the Bachelor thesis of Dennis Aumiller "Mining Relationship Networks from University Websites".<br/>
The thesis and many other resources can be found in this [GitLab repository](https://gitlab.com/dennis.aumiller/HeidelCrawlPublic). <br/>
Additional resources, including several of the used adjacency matrices and the fully crawled dataset (consisting of well over 100 GB of data) reside on the internal storage of the DBS research group ([https://dbs.ifi.uni-heidelberg.de/](https://dbs.ifi.uni-heidelberg.de/)). This is a slightly modified version of the Readme file I wrote for the chair itself, so please apologize for any mentions of internal structures that are not available.

# Tools
This section provides a brief overview over the utilized frameworks and tools for the extraction of the dataset. Namely, a crawling pipeline consisting of [Apache Nutch](http://nutch.apache.org/) and [Apache Solr](http://lucene.apache.org/solr/) was used, in combination with a modified backend in Python3.

## Apache Nutch
Apache Nutch provides a well-matured framework for crawling websites on a scalable level. More recently, the main development focused on two different branches - Nutch 1.X and 2.X; while Nutch 1.X has a close relation to HDFS (Hadoop File System) and Hadoop in general, 2.X moved away from this structure and focused more on flexible back-end options, including NoSQL storage options like Cassandra or MongoDB. <br/>
Due to the fact that the set-up for Nutch 1.X is better documented, easier, as well as the fact that 1.X has a better crawling performance [[1]](http://digitalpebble.blogspot.ca/2013/09/nutch-fight-17-vs-221.html), I chose Nutch 1.X.

### Configuration Files
The main steps and helpful documentation can be found on Nutch's main documentation page [[2]](https://wiki.apache.org/nutch/FrontPage#What_is_Apache_Nutch) and the introductionary tutorial walking you through the main steps in the Nutch crawling pipeline [[3]](https://wiki.apache.org/nutch/NutchTutorial). Note that the explained structure can be automized by the `bin/crawl` script in the respective folder. Also, some of the utilized commands in the tutorial are outdated and should (or can) not be used any longer; Due to a bug in the webgraph building package, I had to base my build on a nightly version of Nutch 1.13/14 from the official Github repository of Nutch [[4]](https://github.com/apache/nutch). The built version of this can either be found on the previously mentioned GitLab repository, or the location on the local file server of the DBS group. This also includes patches to any outdated version changes. <br/>
The main configuration of Nutch is done in several files:
* `conf/nutch-site.xml` This is the main configuration file for the Nutch crawling process. Here, basically any parameter can be changed. I would highly recommend to use the specified setup, as it took me close to two weeks to figure out the granular differences and how to get a decently running configuration up. Some of the important properties are described either in the tutorial or within the file itself; other options are a little trickier to figure out; a list of things I consider most important is mentioned in the next section.
* `conf/schema.xml` This file is important for the later coupling of Nutch and Solr. Mainly, it defines the fields that will be expected by Apache Solr; it has no impact on the crawling process within Nutch, but needs to be copied to Solr.
* `conf/solrindex-mapping.xml` Again, this is important to specify how the interaction between Nutch and Solr is taking place. This should be filled with the fields that should be transferred to Solr, and in some cases needs to be updated if specific plugins are enabled or disabled. Empty fields can cause as much of a problem as fields that "should not be there".
* `conf/regex-urlfilter.txt` This file will be relevant once the `urlfilter-regex` is enabled in the `plugin.includes` option, and can filter out specific URL requests. This is especially important since the default URL filter does ignore URLs with "?", which makes up a sizable portion of sites operated by a CMS (and would subsequently falsify the crawled data). On the other hand, a large portion of dynamic content can be excluded that way, so it has to be filtered by other means. Note that this file is already very extensive and impacts the performance quite a bit, but helps to filter out a lot of dynamic content.
* `urls/seed.txt` This file provides the starting point for any crawl. Certainly, the university's main URL ([http://www.uni-heidelberg.de/](http://www.uni-heidelberg.de/)) should be sufficient for that, but for smaller testing runs, another subselection can be made to test out specific configurations or target towards areas of websites first.

### Configuration at a Glance ###
As mentioned before, the configuration of Nutch is very tedious and feels overwhelming at first. I mention some of the most central aspects here. Generally, the `conf/nutch-default.xml` is a great way to compare to the default options and see where specific changes have been made. <br/>

* `linkdb.ignore.internal.links` For crawling a specific domain, this has to be set to  `false`.
* Basically all `fetcher` properties. These define the crawling speed and concurrency of requests. If set to too many requests, sites might reject or break down. Also note that this can specify how many concurrent fetchers can run. Some advanced options allow Nutch to run in pseudo-distributed mode with mutliple crawling instances, but I did refrain from doing so since it is way harder to debug and configure, but might be worth mentioning for future use.
* `plugin.includes` defines the plugins that are used during the regular crawling cycle. Importantly, by default, a number of plugins are set that are either not compatible with the default `bin/crawl` script or do not work perfectly with Apache Solr.
* `plugin.excludes` similarly works to exclude specific plugins. the elastic-search plugin is incompatible with Solr, and should therefore be disabled if used with Solr instead of Elasticsearch.
* `hadoop.tmp.dir` Sometimes, the temporary folder is restricted in access or not suitable in size for a single iteration. This can be avoided by setting a manual tmp folder for the underlying Hadoop service. Most importantly, this scales the available pages that can be processed with a single iteration, which largely accelerates the crawling process, since it takes less updates of the relatively large crawlDB files.
* `bin/crawl` I also want to highlight an important change in this file: The link inversion step provides a useful option to find anchor tags and in-links to specific sites, but gets ''extremely'' costly for a formidably sized CrawlDB (one iteration takes approximately a whole day on our internal compute server, since it is not very optimized for parallel processing).

## Apache Solr
To make the crawled content available and usable, I used the recommended system by Apache Nutch, which is Apache Solr. Solr provides a search structure over the indexed content, thus allowing users to query for specific terms in specified fields that have been stored by Apache Nutch and passed to Solr. The main network extraction procedure is performed by calling a Solr index repeatedly.

### Configuration
Despite the recommended systems in the Nutch tutorial, which recommends more recent versions of Apache Nutch [[3]](https://wiki.apache.org/nutch/NutchTutorial), I found an older version (4.10.4) to work well with Nutch, since it provides native examples and a sample collection to ease the set up of a working system.<br/>
The most important step in the configuration is the specification of the correct `schema.xml` in `{SOLR-HOME}/example/solr/{Collection-name}/conf`, which can ideally be a direct copy from the pre-defined Nutch version covered in the previous section. The collection folder also holds the data for the search structure. Once everything is configured, the Solr app can be started by running
`java -jar {SOLR-HOME}/example/start.jar` (another reason to prefer Solr 4.X; more recent version complicate the runtime of the instances). Per default, it is accessible via `http://localhost:8983/solr/`.
The respective collections can be reached per RESTful calls to `http://localhost:8983/solr/collection-name`.

## Python Backend
The required packages are listed under the wiki page of the repository [[5]](https://gitlab.com/dennis.aumiller/HeidelCrawlPublic/wikis/home). For Python, the code requires a recommended version of 3.5+, and the plugins ` simplejson, pysolr, SolrClient, requests, lxml, pickle`.

### LSF Extractors
The first part of the Python code is related to the extraction of the LSF's register of persons, from which I extracted a baseline for the later comparisons. Generally, `BipartiteExtractor.py` provides the entry point for the extraction process from scratch. Due to the finite number of pages (i.e. different people and institutes), I chose a very specific but poorly performing extraction method based on the `requests` package, which is more than sufficient for this task. Additionally, note that the URLs of the different institutes and people are relatively easy to extract or manipulate, since they are based on the respective entity IDs. <br/>
The XPath selectors have been manually extracted from sample LSF pages (important note: XPaths extracted with web browsers like Firefox and Chrome return different paths, some of which are not fully correct or working) to extract the basic information of each person. <br/>
Recently (Nov 17), a further extraction path to generate the address information and export to JSON has been added. <br/>
Both entities (institutes and person) have a class defining the information we hope to extract.

The general information extraction pipeline is structured as follows:
* Extract institutes from the LSF institute pages (unlike people, these are extracted from the institute list, since their IDs are too different to crawl iteratively)
* Crawl all possible member IDs from `id=0` to `id=30000` (extended runs up to ID 50000 have not returned more members. The advantage of this method is that it also includes former members of the University, which would otherwise not be listed in the "people index". On the downside, this might lead to duplicates
* Each person is checked for their basic personal information, including the functions they have within the university, and the institutes they work for.
* Afterwards, the institute list is purged by removing institutes that do not appear to have people assigned to them (i.e., no person in the register has this institute listed as their "workplace"). Note that this is an asymmetric comparison and might not be 100% correct; some of the institute pages list people working there, but these people have the institute NOT listed. Due to the inconsistencies in the layout of institute pages, I was not able to extract the other direction, though, so this is an open topic in the current state of the LSF data set.
* Similarly, people are removed that do have exact duplicates in the list of people. Note that this is not only based on first and last name (there are some "accidental" duplicates involved here, too), but only removes people that have ''exactly'' the same information (same institutes, same address, etc.).

The resulting list of people and institutes are stored in the `pickle` format. Note that this requires the files to be read from the same directory as they were crawled from, which explains most of the involved error messages. Additionally, lookup tables for id->name (and vice versa) are stored in JSON format.

### SolrBuilder
Additional libraries: `numpy, matplotlib, itertools, sys, os, multiprocessing, functools`
The SolrBuilder archive is concerned with extracting occurrences of people from the available search index over the University's website. The process works in two iterations: 
* Firstly, the `StorePositions.py` script annotates each page with the occurrences of people. This utilizes Solr's TVRH request tool, which allows responses with parsed content (i.e. split words, including metrics like occurrences, length, position of occurrences, etc.), from which we extract the relevant information for people names, and store them back in a separate field of the Solr Index (this allows partial crawls and a faster access rate than a bulky local processing).
* Secondly, the stored information is then accessed and used to extract the co-occurrences of all the appearing people, according to the theoretical metrics described in the thesis. This functionality is implemented in `OccurrenceBuilder.py`.

Note that there are several files for the analysis of the Solr data located in the same directory. None of them directly affect the extraction pipeline, though, which is why I did not include more documentation on these files. Many of them are self-explanatory by the comments in the code (hopefully).

# Datasets
Provided with the repository are some of the generated networks. Most of them include only some of the basic files, since the adjacency matrix of the extracted co-occurrences are way to large to provide online, but some of them are available internally.

## Crawl Data
The GitLab repository provides two minimal sample crawls and the respective data for Nutch and Solr.
They are similarly named, and can be found within the folder structure of Nutch (`{NUTCH-HOME}/crawl/`) and Solr (`{SOLR-HOME}/example/solr/collection1`) and can be used by renaming them to "crawl" and "data", respectively, and restarting the Solr service, or reloading the collection. <br/>
The fully crawled data is only available internally in the DBS group.

## Webgraph
Aside from the occurrences, I provide a directed graph containing the hyperlink structure of the University website. The sub-graphs of the CS department (`data/webgraph/uni-hd-info.tsv` on the repository), and the whole of the math/physics/computer science subdomain (same folder, `uni-hd-mathphysinfo.tsv`) provide the structure. The full webgraph is available on the internal server, potentially within the SolrBuilder folder.

## People-Institute Network
The bipartite graph and the respective projections are available in the repository, and are located in the folder of the Extractor files.

### LSF structure
The LSF structure information is - aside from the obvious encoding in the network projection and bipartite graph - also available in the form of the pickle-encoded files, which host the available information. See the paragraph on the Extractors for more information.

### Hierarchy
One aspect that has not been mentioned so far is the hierarchy of different chairs and institutes. This has been extracted from the structure provided on the LSF, but so far not really been utilized. A list-nested dictionary of the hierarchy is in the Extractors and data folder of the repository.

## Occurrence Graph
As mentioned, the occurrences are stored to the Solr index, and are extracted as a direct co-occurrence graph from this. Therefore, no explicit occurrence graphs exist as files to be used, but have to be extracted from the index instead.

## Co-Occurrence Matrices
Some of the co-occurrence adjacency matrices are available on the internal storage of the DBS group.

# Analysis

## Accuracy and Evaluation
For the analysis of the graphs, I utilized R together with the package igraph, which is also available in Python, but has shown stronger performance. Additionally, Tore Opsahl has a toolkit that provides an implementation of his network measures for bipartite graphs. It should be mentioned that not all of them worked as expected and yielded very different results. <br/>
Additionally, I performed some analysis of the smaller Person-Institute graph in NetworkX (Python), which also provides some functionalities for bipartite networks, albeit at a slower speed. The main analysis of the extracted co-occurrence matrix. The calculated measures of edge reduced network similarity where computed in Python again. All of these files are located in the Analysis subfolder of the repository. <br/>
As a hint for the co-occurrence matrix, I want to mention that there is a slight misclassification in the occurrences of a single instance, which subsequently has enormously wrong occurrences; this has been manually removed from the graph in the analysis file, and should be considered in its occurrences. The results in the thesis have been corrected accordingly, and are not affected by this value any more.

## Classifier
The used classifier is an implementation of a Support Vector Machine (SVM), built with scikit-learn (Python). A brief hyperparameter optimization has been performed via grid-search. The pages have been classified/extracted, if they contained specific keywords in their URL or content; distributions and availability vary quite a bit, and of course this is only a rough estimate.

# Outstanding Questions and Ideas
* One of the most prominent information deficits is currently the annotation of institutes to specific websites. If it was possible to annotate each website stored in Solr with an additional tag for the institute, the extracted networks could be a lot richer in their respective structure and the networks that could be extracted from this.
* As mentioned before, the information regarding the institutes and assigned people could be updated from both sides, which would most likely be more accurate. Currently, almost a third of initially found institutes is removed during the purging step.
* The utilization of the hierarchy file could also help to structure or classify specific relationships: Are specific institutes on the same level of the hierarchy, or can certain institutes be clustered together? How could this be generalized into a specialized or oblivious hierarchy model? Does a classical clustering algorithm on networks display a similar structure? (I did some quick analysis on this, and initial findings with one of the more common algorithms [I sadly forgot which one it was] returned no useful clusterings)
* The content-type classifier could be trained on the "raw data" of each page, or additional features, and with other kernels, or even neural networks.
* Generally, the classified websites could be extended to a specific set of clusters (e.g., with unsupervised clustering algorithms), to move away from a pre-defined training set, and see if that yields better or worse results, and whether or not there is a "hidden structure" within the crawled data.
* Other, more general ideas are mentioned in the thesis as well.

# Resources
For specific questions about the configuration, data, code, or runs, please feel free to [message me](mailto:Aumiller@stud.uni-heidelberg.de). <br/><br/>

General Links: <br/>
[GitLab repository](https://gitlab.com/dennis.aumiller/HeidelCrawlPublic)<br/>
[http://nutch.apache.org](http://nutch.apache.org)<br/>
[http://lucene.apache.org/solr/](http://lucene.apache.org/solr/)<br/>

<br/> Specific Sources:<br/>
[1] [http://digitalpebble.blogspot.ca/2013/09/nutch-fight-17-vs-221.html](http://digitalpebble.blogspot.ca/2013/09/nutch-fight-17-vs-221.html)<br/>
[2] [https://wiki.apache.org/nutch/FrontPage#What_is_Apache_Nutch](https://wiki.apache.org/nutch/FrontPage#What_is_Apache_Nutch)<br/>
[3] [https://wiki.apache.org/nutch/NutchTutorial](https://wiki.apache.org/nutch/NutchTutorial)<br/>
[4] [https://github.com/apache/nutch](https://github.com/apache/nutch)<br/>
[5] [https://gitlab.com/dennis.aumiller/HeidelCrawlPublic/wikis/home](https://gitlab.com/dennis.aumiller/HeidelCrawlPublic/wikis/home)<br/>

