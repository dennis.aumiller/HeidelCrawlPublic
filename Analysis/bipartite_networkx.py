#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 24 21:08:16 2017

@author: Dennis Aumiller

"""



import networkx as nx
from networkx.algorithms import bipartite
import operator
import json
import math
import pprint
import pickle
import collections

def Hierarchy():
    return collections.defaultdict(Hierarchy)

def hierarchy_reader(fn="hierarchy.list"):
    with open(fn, 'rb') as f:
        hierarchy = pickle.load(f)
    return hierarchy

def hierarchy_depth(d, level=1):
    if not isinstance(d, dict) or not d:
        return level
    return max(hierarchy_depth(d[k], level + 1) for k in d)

def cc_shit(B):
    n_cc = nx.connected_components(B)
    n_cc = list(n_cc)
    a_n_cc = [el for el in n_cc if len(el)>1]
    b_n_cc = [el for el in a_n_cc if len(el)>2]
    largest_cc = max(nx.connected_component_subgraphs(B),  key=len)
    fraction_cc = len(largest_cc) / len(B)
    print("Connection of largest component: {}".format(fraction_cc))
    print("Elements in largest component: {}".format(len(largest_cc)))
    print("Total number of connected components: {}".format(len(n_cc)))
    print("CCs with at least 2 elements: {} / CCs with at least 3 elements: {}".format(len(a_n_cc), len(b_n_cc)))
    return n_cc, a_n_cc, b_n_cc, largest_cc, fraction_cc

if __name__ == "__main__":
    
    B = nx.Graph()
    
    
    with open("people_list.tsv", "r") as f:
        x = f.readlines()
        people = [0] * len(x)
        for i, line in enumerate(x):
            people[i] = line.strip()
            
    with open("institute_list.tsv", "r") as f:
        x = f.readlines()
        institutes = [0] * len(x)
        for i, line in enumerate(x):
            institutes[i] = line.strip()
    
    with open("bipartite_edgelist.tsv", "r") as f:
        x = f.readlines()
        edges = [0] * len(x)
        for i, line in enumerate(x):
            edges[i] = tuple(line.strip().split("\t"))
            
    B.add_nodes_from(people, bipartite=0)
    B.add_nodes_from(institutes, bipartite=1)
    B.add_edges_from(edges)
    
    num_people = len(people)
    num_institutes = len(institutes)
    num_edges = len(edges)
    
    print("Number of people: {}".format(num_people))
    print("Number of institutes: {}".format(num_institutes))
    print("Number of connections: {}".format(num_edges))
    
    print("Resulting average connections per person: {}".format(num_edges/num_people))
    print("Number of connections per institute: {}".format(num_edges/num_institutes))
    print("Number of people assigned at least one institute: {}".format(15038))
    
    
    density = round(bipartite.density(B, people),10)
    print("The density of the graph is: {}".format(density))

    d_centrality = bipartite.degree_centrality(B, institutes)
    top_k = sorted(d_centrality.items(), key=operator.itemgetter(1), reverse=True)[:50]
    top_10_p = []
    top_10_i = []
    
    # read the people dictionary lookup
    with open("lookup_people.json", 'r') as f:
        people_lookup = json.load(f)
        
    with open("lookup_institutes.json", 'r') as f:
        institute_lookup = json.load(f)
    
    for i, tup in enumerate(top_k):
        if tup[0][0]=='i' and len(top_10_i) < 10:
            # 22225 is outlier whatever shit
            top_10_i.append([tup[0], institute_lookup[tup[0]], math.trunc(tup[1]*22225)])
        elif tup[0][0]=='p' and len(top_10_p) < 10:
            top_10_p.append([tup[0], people_lookup[tup[0]],math.trunc(tup[1]*len(institutes))])
    print("The following are the ten largest institutes, ranked by members:")
    for line in top_10_i:
        print(line)
    print("The following are the ten most associated members:")
    for line in top_10_p:
        print(line)
    print("Calculated the degree centrality of both institutes and people")


    ### CONNECTED COMPONENT SHIT ###
    print("Note that all institutes do require at least one connection, thus automatically connected!")
    n_cc, a_n_cc, b_n_cc, largest_cc, fraction_cc = cc_shit(B)
    
    ### DIAMETER ###    
#    diameter = nx.diameter(largest_cc)
    # diameter is 23
#    print("The diameter of the largest connected component is: {}".format(diameter))
    print("The diameter of the largest connected component is: {}".format(23))
    
    ### COMPARE TO HIERARCHY DEPTH ###
    hierarchy = hierarchy_reader()
    max_depth = hierarchy_depth(hierarchy)
    print("The estimated diameter from the hierarchy is: {}".format(2*max_depth))
    
    

#    avg_lcc_bp = bipartite.average_clustering(B)
#    print("The average local bipartite clustering coefficient: {}".format(avg_lcc_bp))
    
    G = bipartite.collaboration_weighted_projected_graph(B, people)
    print("Computed the weighted projection") 
    
    ### AVERAGE DEGREE ###
    vertices = G.number_of_nodes()
    edges = G.number_of_edges()
    print("The one-mode projection has {} vertices (obv), and {} edges".format(vertices, edges))
      
    avg_deg = edges / vertices
    print("The average degree of a node is: {}".format(avg_deg))
    
    ### DENSITY OF THE GRAPH ###
    dense = nx.density(G)
    print("THe density of the graph is: {}".format(dense))
    
    ### WEIGHTED DEGREE CENTRALITY VS DEGREE CENTRALITY ###
    d_centralityG = nx.degree_centrality(G)
    G_top = sorted(d_centralityG.items(), key=operator.itemgetter(1), reverse=True)[:10]
    
    G_top_10 = []
    
    for i, tup in enumerate(G_top):
        G_top_10.append([tup[0], people_lookup[tup[0]], math.trunc(tup[1]*(vertices-1))])
    print("The following are the 10 most connected members:")
    for line in G_top_10:
        print(line)
    
    print("Weighted degree centrality on the other hand:")
    degrees = G.degree(weight='weight')
    G_top_weighted = sorted(degrees.items(), key=operator.itemgetter(1), reverse=True)[:10]
    G_top_weighted_10 = []
    for i, tup in enumerate(G_top_weighted):
        G_top_weighted_10.append([tup[0], people_lookup[tup[0]], math.trunc(tup[1])])
    print("The following are the 10 most influencial members:")
    for line in G_top_weighted_10:
        print(line)
        
#    avg_lcc = nx.average_clustering(G)
#    print("The average local clustering coefficient of the projection is: {}".format(avg_lcc))
#    gcc = nx.transitivity(G)
#    print("The global clusterin coefficient for the projection is: {}".format(gcc))
    
#    The average local clustering coefficient of the projection is: 0.620362642645237
    print("The average local clustering coefficient of the projection is: 0.620362642645237")
#    The global clusterin coefficient for the projection is: 0.9701644629340285
    print("The global clusterin coefficient for the projection is: 0.9701644629340285")

    n_ccG, a_n_ccG, b_n_ccG, largest_ccG, fraction_ccG = cc_shit(G)
#    diameter_G = nx.diameter(largest_ccG) # 11
    print("The calculated diameter of this projection is: {}".format(11))
    
#    between_G = nx.betweenness_centrality(G, normalized=False)
#    G_top_between = sorted(between_G.items(), key=operator.itemgetter(1), reverse=True)[:10]
#    G_top_between_pretty = []
#    for i, tup in enumerate(G_top_between):
#        G_top_between_pretty.append([tup[0], people_lookup[tup[0]], math.trunc(tup[1])])
    print("Calculated the betweenness centrality for the network.")
#    print("The 10 most influcencial are here:")
#    for line in G_top_between_pretty:
#        print(line)
        
    print("""    Calculated the betweenness centrality for the network.
    The 10 most influcencial are here:
    ['p08985', 'Bernd Schneidmüller', 6079520]
    ['p07858', 'Thomas Rausch', 5684516]
    ['p24869', 'Jürgen Pahle', 4389804]
    ['p01055', 'Michael Boutros', 4042712]
    ['p12151', 'Angela Stevens', 3190302]
    ['p06027', 'Volker Lindenstruth', 3127613]
    ['p21103', 'Antje Hartje', 3088000]
    ['p08772', 'Christian Schmahl', 2949867]
    ['p05422', 'Hans-Georg Kräusslich', 2618850]
    ['p08712', 'Peter Schirmacher', 2588792]
""")
    
    # extremely high. Argue from creation of connected network?
#    r_G = nx.assortativity.degree_assortativity_coefficient(G)
    # 0.92664511920794612
    print("The assortativity coefficient is: {}".format(0.92664511920794612))
    
    
    
    
    
    
    
