#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 25 21:08:30 2017

@author: dennis
"""


from sklearn import preprocessing
from sklearn import svm
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import recall_score, precision_score
import numpy as np


def report(results, n_top=3):
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
                  results['mean_test_score'][candidate],
                  results['std_test_score'][candidate]))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")

if __name__ == "__main__":
    
    x_naive = np.load("x_train.npy")
    y = np.load("y.npy")
    
    x_naive = preprocessing.scale(x_naive)
    
    clf = svm.SVC(kernel='rbf', gamma=10,class_weight='balanced', C=1000)

    scores = cross_val_score(clf, x_naive, y, cv=5)
    
    with open("results_4.txt", "w") as f:
        f.write("The results for the 5-fold cross validation of 4 categories:\n")
        f.write(str(scores)+"\n")
    
    print(scores)
    
    x_binary = np.load("x_binary.npy")
    y_binary = np.load("y_binary.npy")
    
    x_binary = preprocessing.scale(x_binary)
    
    clf2 = svm.SVC(kernel='rbf', gamma=10,class_weight='balanced', C=1000)

    scores_binary = cross_val_score(clf2, x_binary, y_binary, cv=5)
    
    print(scores_binary)   
    with open("results_binary.txt", "w") as f:
        f.write("The results for the 5-fold cross validation of binary categories with publications:\n")
        f.write(str(scores_binary)+"\n")
        

    
    
    
    # doesn't work
#    X_train, X_test, y_train, y_test = train_test_split(x_naive, y, test_size=0.2, random_state=0)
# 
#    tuned_parameters = [{'kernel': ['rbf'], 'gamma': [1e-3, 1e-4],
#                     'C': [1, 10, 100, 1000]},
#                    {'kernel': ['linear'], 'C': [1, 10, 100, 1000]}]
#
#    scores = ['precision', 'recall']
#
#    for score in scores:
#        print("# Tuning hyper-parameters for %s" % score)
#        print()
#    
#        clf = GridSearchCV(svm.SVC(C=1), tuned_parameters, cv=5,
#                           scoring='%s_macro' % score)
#        clf.fit(X_train, y_train)
#    
#        print("Best parameters set found on development set:")
#        print()
#        print(clf.best_params_)
#        print()
#        print("Grid scores on development set:")
#        print()
#        means = clf.cv_results_['mean_test_score']
#        stds = clf.cv_results_['std_test_score']
#        for mean, std, params in zip(means, stds, clf.cv_results_['params']):
#            print("%0.3f (+/-%0.03f) for %r"
#                  % (mean, std * 2, params))
#        print()
#    
#        print("Detailed classification report:")
#        print()
#        print("The model is trained on the full development set.")
#        print("The scores are computed on the full evaluation set.")
#        print()
#        y_true, y_pred = y_test, clf.predict(X_test)
#        print(classification_report(y_true, y_pred))
#        print()