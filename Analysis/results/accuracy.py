#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  1 10:48:32 2017

@author: Dennis Aumiller
"""

import json
import numpy as np
from operator import itemgetter

def recaller(fn):
    with open("people_projection.tsv", "r") as f:
        lines = f.readlines()
        edgelist_pi = [0] * 2*len(lines)
        length = len(lines)
        for i,line in enumerate(lines):
            temp = line.strip().split("\t")[0:2]
            # ggf add hashing here
            edgelist_pi[i] = (temp[0], temp[1])
            edgelist_pi[i+length] = (temp[1], temp[0])
#            edgelist_pi[i] = (temp[0][1:], temp[1][1:])
            
#    edgelist_pw = []
    # either read from igraph edgelist, or numpy directly
    with open(fn, "r") as f:
        gen_lines = f.readlines()
        edgelist_pw = [0] * len(gen_lines)
        w_edgelist_pw = [0] * len(gen_lines)
        for i, line in enumerate(gen_lines):
            temp = line.strip().split("\t")
            src ="p" + "{0:05d}".format(inv_ind_lookup[temp[0]])
            trg = "p" + "{0:05d}".format(inv_ind_lookup[temp[1]])
            w_edgelist_pw[i] = [src, trg, float(temp[2])]
            edgelist_pw[i] = (src, trg)
    
    
    print("Length of reference is {}".format(len(edgelist_pi)))
    print("Length of compared edgelist is {}".format(len(edgelist_pw)))
    
    set_pi = set(edgelist_pi)
    set_pw = set(edgelist_pw)
    
    inter = set_pi.intersection(set_pw)
    un = set_pi.union(set_pw)
    acc = len(inter)/len(un)
    print("The accuracy is at {:.2f}%".format(acc*100))
    
    rec1 = len(inter)/len(set_pi)
    rec2 = len(inter)/len(set_pw)
    print("Recall PI is {:.2f}%".format(rec1*100))
    print("Recall PW is {:.2f}%".format(rec2*100))
    
    # Reduction
    w_edgelist_pw.sort(key=itemgetter(2),reverse=True)
    
    reduced_pw = [0] * len(edgelist_pi)
    for i in range(len(edgelist_pi)):
        reduced_pw[i] = (w_edgelist_pw[i][0], w_edgelist_pw[i][1])
    reduced_set = set(reduced_pw)
    inter_red = set_pi.intersection(reduced_set)
    un_red = set_pi.union(reduced_set)
    acc_red = len(inter_red)/len(un_red)
    rec_red = len(inter_red)/len(set_pi)
    
    print("The accuracy for a reduced set is {:.2f}%".format(acc_red*100))
    print("The reduced recall is {:.2f}%".format(rec_red*100))


if __name__ == "__main__":
    
    # load old
    with open("lookup.json", 'r') as f:
        ind_lookup = json.load(f)
        
    with open("inv_lookup.json", 'r') as f:
        inv_ind_lookup = json.load(f)
        
        
    # inaccurate LSF?
    # overshadowed "work relationship"
    # barely inter-institute connections?
    print("--------------------------------------------")
    print("STARTING WITH BASELINE MODEL")
    recaller("baseline.edge")
    print("--------------------------------------------")
    print("STARTING WITH LINEAR LIW MODEL")
    recaller("linear.edge")
    print("--------------------------------------------")
    print("STARTING WITH EXP LIW MODEL")
    recaller("decay.edge")
    print("--------------------------------------------")
    print("STARTING WITH PURE OCC MODEL")
    recaller("pure.edge")
    print("--------------------------------------------")
    print("STARTING WITH UNLIMITED LINEAR MODEL")
    recaller("unlimited.edge")

        

    

            
    
    