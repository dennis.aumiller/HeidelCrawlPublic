Rscript analyzer.R /home/daumiller/adj_attention_decay_edgelist.npy short attention_decay &
Rscript analyzer.R /home/daumiller/adj_attention_linear_edgelist.npy short attention_linear &
Rscript analyzer.R /home/daumiller/adj_baseline_occ_edgelist.npy short baseline_occ &
Rscript analyzer.R /home/daumiller/adj_people_co_occ_edgelist.npy short co_occ &
Rscript analyzer.R /home/daumiller/adj_pure_occ_edgelist.npy short pure_occ &
Rscript analyzer.R /home/daumiller/adj_unlimited_linear_edgelist.npy short unlimited &
