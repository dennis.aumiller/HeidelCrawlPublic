library(igraph)
library(RcppCNPy)
library(rjson)
library(hashmap)
arg <- commandArgs(trailingOnly = TRUE)
mat <- arg[1]
runlen <- arg[2]
name <- arg[3]

fn <- paste("/home/daumiller/ba/", name, ".txt", sep="")

# read graph adjacency matrix and create igraph object
if(is.na(mat)) {
  adj_mat <- npyLoad("/home/daumiller/adj_attention_decay_edgelist.npy")
} else {
  adj_mat <- npyLoad(mat)
}
# note that this is a weighted, directed graph
G <- graph_from_adjacency_matrix(adjmatrix = adj_mat, mode = "directed", weighted = TRUE)

# lookup
#ind_lookup <- fromJSON(file="/home/dennis/HeidelCrawl/Analysis/results/lookup.json")
ind_lookup <- fromJSON(file="/home/daumiller/ba/lookup.json")
keys_ind <- names(ind_lookup)
values_ind <- unlist(ind_lookup, use.names = FALSE)
values_ind <- values_ind -1
ind_hash <- hashmap(keys_ind, values_ind)

#inv_ind_lookup <- fromJSON(file="/home/dennis/HeidelCrawl/Analysis/results/inv_lookup.json")
inv_ind_lookup <- fromJSON(file="/home/daumiller/ba/inv_lookup.json")
keys_inv <- names(inv_ind_lookup)
keys_inv <- as.numeric(keys_inv)
keys_inv <- keys_inv + 1
keys_inv <- paste(keys_inv)
values_inv <- unlist(inv_ind_lookup, use.names = FALSE)
values_inv <- paste(values_inv)
inv_hash <- hashmap(keys_inv, values_inv)

#name_lookup <- fromJSON(file="/home/dennis/HeidelCrawl/Analysis/results/lookup_people.json")
name_lookup <- fromJSON(file="/home/daumiller/ba/lookup_people.json")
keys_name <- names(name_lookup)
values_name <- unlist(name_lookup, use.names = FALSE)
values_name <- paste(values_name)
name_hash <- hashmap(keys_name, values_name)

# inv_ind_lookup['15930']
# Out[19]: 21407
# ignore fucking "Abteilung Spanisch" Gastdozent
id_gast <- 15930+1
G <- delete_vertices(G, id_gast)

# basics
num_vertices <- length(V(G))
num_edges <- length(E(G))
write(paste("The graph contains", num_vertices, "vertices and", num_edges, "edges"), file=fn, append = TRUE, ncolumns = 1)

# max degree, and basic calculations
d <- degree(G, mode="all")

# top 10 nodes
key <- name_hash[[inv_hash[[paste(order(d, decreasing = TRUE)[1:10])]]]]
val <- d[order(d, decreasing = TRUE)[1:10]]
top_10 <- data.frame(key, val)
write("The following are the nodes with the highest degree:", file=fn, append = TRUE, ncolumns = 1)
write.table(top_10, file=fn, append=TRUE)

# non-zero elements:
non_zero <- length(d[which(d>0)])
write(paste("The graph contains", non_zero, "non_zero vertices"), file=fn, append = TRUE, ncolumns = 1)

#[1] 18218

# deg dist
# deg_dist <- degree_distribution(G, cumulative = FALSE)
# x <- seq(from = 1, to = length(deg_dist))
# png(filename = paste("/home/daumiller/deg_dist_", name, ".png", sep=""))
# plot( x, deg_dist, log = "xy", main = paste("Log-log degree distribution", name) )
# dev.off()
# deg_dist_cum <- degree_distribution(G, cumulative = TRUE)
# png(filename = paste("/home/daumiller/deg_dist_cum_", name, ".png", sep=""))
# lines(x, deg_dist_cum, log="xy", main=paste("Log-log cumulative degree distribution", name) )
# dev.off()

d_w <- strength(G, mode="all")
key_w <- name_hash[[inv_hash[[paste(order(d_w, decreasing = TRUE)[1:10])]]]]
val_w <- d_w[order(d_w, decreasing = TRUE)[1:10]]
#plot_w <- d_w[order(d_w, decreasing = FALSE)]
top_10_w <- data.frame(key_w, val_w)
write("The following are the nodes with the highest weighted degree:", file=fn, append = TRUE, ncolumns = 1)
write.table(top_10_w, file=fn, append=TRUE)

d_w_in <- strength(G, mode="in")
key_w_in <- name_hash[[inv_hash[[paste(order(d_w_in, decreasing = TRUE)[1:10])]]]]
val_w_in <- d_w_in[order(d_w_in, decreasing = TRUE)[1:10]]
#plot_w_in <- d_w_in[order(d_w_in, decreasing = FALSE)]
top_10_w_in <- data.frame(key_w_in, val_w_in)
write("The following are the nodes with the highest weighted in-degree:", file=fn, append = TRUE, ncolumns = 1)
write.table(top_10_w_in, file=fn, append=TRUE)

d_w_out <- strength(G, mode="out")
key_w_out <- name_hash[[inv_hash[[paste(order(d_w_out, decreasing = TRUE)[1:10])]]]]
val_w_out <- d_w_out[order(d_w_out, decreasing = TRUE)[1:10]]
#plot_out <- d_w_out[order(d_w_out, decreasing = FALSE)]
top_10_w_out <- data.frame(key_w_out, val_w_out)
write("The following are the nodes with the highest weighted out-degree:", file=fn, append = TRUE, ncolumns = 1)
write.table(top_10_w_out, file=fn, append=TRUE)

# bad idea
# plot(G)

# clustering coefficients
gcc <- transitivity(G, type="global")
# 0.2309686
write(paste("The global clustering coefficient is", gcc), file=fn, append = TRUE, ncolumns = 1)

lcc <- transitivity(G, type="local", isolates="zero")
avg_lcc <- mean(lcc)
write(paste("The average local clustering coefficient is", avg_lcc), file=fn, append = TRUE, ncolumns = 1)
lcc_wo <- transitivity(G, type="local", isolates="NaN")
lcc_wo <- na.omit(lcc_wo)
avg_lcc_wo <- mean(lcc_wo)
write(paste("The average local clustering coefficient without the zero degree nodes is", avg_lcc_wo), file=fn, append = TRUE, ncolumns = 1)

# assortativity measure
ass <- assortativity_degree(G, directed=FALSE)
write(paste("The degree assortativity coefficient for the graph is", ass), file=fn, append = TRUE, ncolumns = 1)

# fraction of largest connected component
cl <- clusters(G, mode = "weak")
largest_cc <- max(cl$csize)/length(V(G))
num_cc <- cl$no
write(paste("The largest connected component contains", largest_cc, "of the total graph."), file=fn, append = TRUE, ncolumns = 1)
write(paste("The graph is split into", num_cc, "connected components"), file=fn, append = TRUE, ncolumns = 1)
# density
density <- edge_density(G, loops = FALSE)
write(paste("The density of the graph is", density), file=fn, append = TRUE, ncolumns = 1)

# calculate undirected and unweighted diameter
diam <- diameter(G, directed=FALSE, weights = NA)
write(paste("The unweighted (undirected) diameter of the graph is", diam), file=fn, append = TRUE, ncolumns = 1)

# betweenness scores
H <- as.undirected(G, mode = "collapse")
H <- delete_edge_attr(H, "weight")
betw <- betweenness(H, directed=FALSE, weights = NULL, normalized = FALSE)
key_betw <- name_hash[[inv_hash[[paste(order(betw, decreasing = TRUE)[1:10])]]]]
val_betw <- betw[order(betw, decreasing = TRUE)[1:10]]
top_10_betw <- data.frame(key_betw, val_betw)
write.table(top_10_betw, file=fn, append=TRUE)

# PageRank
pr <- page_rank(G, directed=TRUE)
key_pr <- name_hash[[inv_hash[[paste(order(pr$vector, decreasing = TRUE)[1:10])]]]]
val_pr <- pr$vector[order(pr$vector, decreasing = TRUE)[1:10]]
top_10_pr <- data.frame(key_pr, val_pr)
write.table(top_10_pr, file=fn, append = TRUE)

