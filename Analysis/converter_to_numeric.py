#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 24 19:21:19 2017

@author: Dennis Aumiller
"""

import numpy as np
import csv

if __name__ == "__main__":
#    edgelist = np.genfromtxt("/home/dennis/HeidelCrawl/Analysis/bipartite_edgelist.tsv", dtype=object,delimiter="\t")
    with open("/home/dennis/HeidelCrawl/Analysis/bipartite_edgelist.tsv", "r") as f:
        dummy = csv.reader(f, delimiter="\t")
        edgelist = list(dummy)
    
    for i, row in enumerate(edgelist):
        row[0] = row[0][1:]
        row[1] = row[1][1:]
        
    with open("/home/dennis/HeidelCrawl/Analysis/numeric_bipartite.tsv", "w") as f:
        for i, row in enumerate(edgelist):
            f.write(row[0]+"\t"+row[1]+"\n")
    