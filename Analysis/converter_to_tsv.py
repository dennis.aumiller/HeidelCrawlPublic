#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 24 16:26:15 2017

@author: Dennis Aumiller
"""

import numpy as np
import sys

# Transfer files to CSV/TSV files

if __name__ == "__main__":
    
    args = sys.argv
    if(len(args)>1):
        filename = args[1]
    
    pickled = np.load(filename)
